# Requirements

* Java 1.8
    * please set JAVA_HOME environment variable 
* Gradle
* Bower
    * Node
    * NPM
# Database

1.Create 2 dbs `skyplus` and `skyplus_test`

# How to run

## Console/terminal Commands

1. `bower install`
1. `gradle clean bootRun`

## IntelliJ
 
1. import this project into IntelliJ by select build.gradle or the folder that is containing  build.gradle  file.
1. `bower install`
1. run Application that has main method.

## Packagin
1. `gradle package`
1. `java -jar target/skyplus-1.0-SNAPSHOT.war
 
