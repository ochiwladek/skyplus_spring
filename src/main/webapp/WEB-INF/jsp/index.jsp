<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>


    <style rel="stylesheet">

       #next{
        height:499px;
       }
        img {
            width: 100%;
            height: auto;
        }

        button {
            position: absolute;
        }

        .next {
            right: 5px;
        }

        .prev {
            left: 5px;
        }
    </style>
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>

<div class="row">


 <div class="slider" id="mainslider">
    <ul class="slides">
      <li>
        <img src="/resources/images/nairobi.jpg"> <!-- random image -->
        <div class="caption center-align" id="maincaption">
          <h3 align="center" id="skyplus"><span class="sky">Sky</span> <span class="plus">Plus</span></h3>
  <h4 align="center">Real Estate consultancy firm</h4>
        </div>
      </li>
     
     
     
       <li>
        <img src="/resources/images/nairobi_night.jpg"> <!-- random image -->
        <div class="caption center-align" id="maincaption">
         <h3 align="center" id="skyplus"><span class="sky">Sky</span> <span class="plus">Plus</span></h3>
          <h4 align="center">Real Estate consultancy firm</h4>
        </div>
      </li>
    </ul>
  </div>

</div>


<div class="row" id="latest">

 
 <div class="col offset-m1 m10">
<h4 align="center" id="darkblue">Latest Property</h4>
     <c:forEach items="${propertyPage.content}" var="property">
         <div class="col m4 s12">
             <div class="card small" >
                 <div class="card-image">
                     <img src="${property.asset.name}">
                     <span class="card-title"></span>
                 </div>
                 <div class="card-content">
                     <p>${property.name}</p>
                    


                     <p>Cost : ${property.cost}</p>

                 </div>
                 <div class="card-action">
                     <a href="/front/property/show/${property.id}" class="waves-effect waves-light btn" id="darkbluebtn">View</a>
                 </div>
             </div>
         </div>
     </c:forEach>
  </div>
    </div>
<div class="row">
    <jsp:include page="/WEB-INF/jsp/common/pagination.jsp">
        <jsp:param name="paginatedRecord" value="propertyPage"/>
        <jsp:param name="url" value="${pagenatedUrl}"/>
    </jsp:include>
</div>
<!-- jquery ui -->
<script type="text/javascript" src="/resources/components/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript" src="/resources/js/custom/frontSearch.js"></script>




  

  
  
<script type="text/javascript">

$(document).ready(function(){

     $('.slider').slider();
});

</script> 
 
 






<%--end body--%>
<!-- start of footer section -->



<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<!-- Include all compiled plugins (below), or include individual files as needed -->

</html>
