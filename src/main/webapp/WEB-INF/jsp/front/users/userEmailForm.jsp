<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
        <jsp:param name="title" value="Login"/>
    </jsp:include>
    <!-- Custom styles for this template -->
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>
<div class="container" id="content">

<div class="row">
    <form:form class="col offset-s2 s6" id="lgform" action="/front/account/password-recovery" method="post" modelAttribute="passwordRecovery">
        <h4 align="center">Submit Email To recover Password</h4>

        <div class="row">
            <div class="input-field col s12">
                <i class="mdi-communication-email prefix"></i>
                <%--<input id="icon_prefix" type="text" class="validate" id="loginId">--%>
                <form:input path="email" type="email" id="icon_prefix" class="validate" />
                <form:errors path="email" />
                <label class="active" for="icon_prefix">Email</label>
            </div>
            <div class="input-field col s12">
                <i class="mdi-communication-email prefix"></i>
                <%--<input id="icon_prefix" type="text" class="validate" id="email" type="email">--%>
                <form:input path="email2" type="email" class="validate" id="icon_prefix" />
                <form:errors path="email2" />
                <label class="active" for="icon_prefix">Confirm Email</label>
            </div>
            <div class="input-field col offset-s4 s8">
                <button class="btn waves-effect waves-light" type="submit" name="action" id="skyplusbtn">Submit
                    <i class="mdi-content-send right"></i>
                </button>
            </div>
        </div>
        <br>
    </form:form>
  </div>

</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<%--footer--%>
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<%--end of footer--%>
</body>
</html>
