<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<head>
    <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
        <jsp:param name="title" value="Sign Up"/>
    </jsp:include>
    <!-- Custom styles for this template -->
    <style type="text/css">

    #successpage{
        height:500px;
    }


    </style>
</head>

<body>

<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>

<div class="container" id="content">
 <div class="row" id="successpage">
 
     <div class="col offset-m6 m6 s12 l6">
         <h3>Sign up was successful.</h3>
         <p>An account activation link has been sent to your email.
             Please click on the link to activate your account</p>
     </div>


    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->

<%--footer--%>
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<%--end of footer--%>
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
