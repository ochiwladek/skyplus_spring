<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
    <jsp:param name="title" value="Skyplus"/>
  </jsp:include>
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>


  
    <div class="row" id="contactemail">
     
      <div class="col offset-m3 m6" id="contactusbox">
       <h3 align="center">Email Us</h3>
        <c:if test="${success == true}">
          <div class="card-panel" role="alert">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>${message}</strong> .
          </div>
        </c:if>
        <form:form acceptCharset="UTF-8" action="/front/contactUs" method="post" modelAttribute="contactUsDetails" cssClass="form-horizontal" role="form">

          <div class="input-field">
           
            <div class="col m9">
              <form:input path="name" id="name" type="text" cssClass="form-control" placeholder="First Name" />

              <form:errors path="name" cssClass="form-inline" />
            </div>
          </div>
          <div class="input-field">
          
            <div class="col m9">
              <form:input path="fromEmail" id="fromEmail" type="email" cssClass="form-control" placeholder="Email"/>
              <form:errors path="fromEmail" cssClass="form-inline" />
            </div>
          </div>
          <div class="input-field">
          
            <div class="col m9">
              <form:input path="title" id="title" type="text" cssClass="form-control" placeholder="Message Title" />
              <form:errors path="title" cssClass="form-inline" />
            </div>
          </div>
          <div class="input-field">
           
            <div class="col m9">
              <form:textarea path="message" id="description" type="text" cssClass="materialize-textarea"
                             placeholder="Message" />
              <form:errors path="message" cssClass="form-inline" />
            </div>
          </div>
          <div class="input-field">
            <div class="col offset-m3 col m10">
              <input class="btn btn-success" type="submit" value="Send email">
            </div>
          </div>
        </form:form>
      </div>
  
 
</div>
<!-- end of row -->

<%--end body--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</html>
