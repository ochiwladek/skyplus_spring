<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<head>
    <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
        <jsp:param name="title" value="Sign Up"/>
    </jsp:include>
    <!-- Custom styles for this template -->
</head>

<body>

<jsp:include page="/WEB-INF/jsp/common/adminTopmenu.jsp"/>

<div class="container" id="content">
 <div class="row">
 
     <%--<form class="col offset-s2 s6" id="lgform" action="/users/signup" method="post" modelAttribute="user">--%>
     <form:form class="col offset-s2 s6" id="lgform" action="/users/signup" method="post" modelAttribute="user">
         <h4 align="center">Sign Up</h4>

         <div class="row">
             <div class="input-field col s12">
                 <i class="mdi-action-account-circle prefix"></i>
                 <%--<input id="icon_prefix" type="text" class="validate" id="loginId">--%>
                 <form:input path="loginId" type="text" id="icon_prefix" class="validate" />
                 <form:errors path="loginId" />
                 <label class="active" for="icon_prefix">Username</label>
             </div>
             <div class="input-field col s12">
                 <i class="mdi-communication-email prefix"></i>
                 <%--<input id="icon_prefix" type="text" class="validate" id="email" type="email">--%>
                 <form:input path="email" type="text" class="validate" id="icon_prefix" />
                 <form:errors path="email" />
                 <label class="active" for="icon_prefix">Email</label>
             </div>
             <div class="input-field col s12">
                 <i class="mdi-action-lock prefix"></i>
                 <%--<input id="icon_lock" type="tel" class="validate" id="password" type="password">--%>
                 <form:input path="password" type="password" id="icon_lock" class="validate" />
                 <form:errors path="password" />
                 <label class="active" for="icon_lock">Password</label>
             </div>
             <div class="input-field col s12">
                 <i class="mdi-action-lock prefix"></i>
                 <%--<input id="icon_lock" type="tel" class="validate" id="password2" type="password">--%>
                 <form:input path="password2" id="icon_lock" class="validate" type="password" />
                 <form:errors path="password2" />
                 <label class="active" for="icon_lock">Confirm Password</label>
             </div>
             <div class="input-field col offset-s4 s8">
                 <button class="btn waves-effect waves-light" type="submit" name="action" id="skyplusbtn">Register
                     <i class="mdi-content-send right"></i>
                 </button>
             </div>
         </div>
         <br>
     </form:form>
    <%--</form>--%>


    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->

<%--footer--%>
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<%--end of footer--%>
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
