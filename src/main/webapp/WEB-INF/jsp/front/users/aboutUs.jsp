<!DOCTYPE html>
<html lang="en">
<head>
  <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
    <jsp:param name="title" value="Skyplus"/>
  </jsp:include>
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>


<div class="row">
<div class="slider" id="mainslider">
    <ul class="slides">
      <li>
        <img src="/resources/images/nairobi.jpg"> <!-- random image -->
        <div class="caption center-align" id="maincaption">
         
  <h2 align="center" id="skyplus"><span>About Us</span></h2>
  <div>
   <a href="#mv"><i class="fa fa-chevron-circle-down fa-5x" id="scrolldown"></i></a>
   </div>
        </div>
      </li>
     
     
     
       <li>
        <img src="/resources/images/kimathi.jpg"> <!-- random image -->
        <div class="caption center-align" id="maincaption">
        <h2 align="center" id="skyplus"><span id="">About Us</span></h2>
        <div>
   <a href="#mv"><i class="fa fa-chevron-circle-down fa-5x" id="scrolldown"></i></a>
   </div>
        </div>
      </li>
    </ul>
  </div>
 
</div>
<div class="row" id="mv">
<div class="col m6 s12">
<div class="container">
<div id="newabout">
<h4 align="center" id="darkblue">Sky Plus Agencies</h4>
<p>Sky Plus Agencies is a Real Estate consultancy firm registered under the Laws of Kenya.  The firm engages in Property Letting, Management and Sale in Nairobi and its environs.</p>
<p>It was founded in 2009 and incorporated in the year 2012 and has been providing professional services since then as its business portfolio continues to expand.</p>
</div>
</div>
 
  </div><!-- end of col-md -->
<div class="col m5 s12">


 <ul class="collapsible" data-collapsible="accordion">
    <li>
      <div class="collapsible-header active"><h4 id="lightblue">Mission</h4></div>
      <div class="collapsible-body"><p>To become Kenya's leading provider of first-hand information and solutions in the Real Estate Industry with a lasting impact on our clients. </p></div>
    </li>
   
   
  </ul>
  <ul class="collapsible" data-collapsible="accordion">
    
    <li>
      <div class="collapsible-header active"><h4 id="lightblue">Vision</h4></div>
      <div class="collapsible-body"><p>To create a distinguished customer loyalty and bring value to our clients using the most innovative technology</p></div>
    </li>
   
  </ul>
  </div>
  
  </div>

  <div class="row" id="values">
   <h3 align="center" id="darkblue">Our Values</h3>

     <div class="col offset-m5 s12">

     <ul id="valueslist">
     <li><p><i class="fa fa-check fa-fw"></i>We listen to you</p></li>
     <li><p><i class="fa fa-check fa-fw"></i>We work with you</p></li>
     <li><p><i class="fa fa-check fa-fw"></i>We respect you</p></li>
     <li><p><i class="fa fa-check fa-fw"></i>We are open and honest with you</p></li>
     <li><p><i class="fa fa-check fa-fw"></i>We are accountable to you</p></li>
     </ul>

     </div>

  </div>



<div class="row" id="propertymgmt">
<div class="container">

  <h3 align="center" id="darkblue">Property Management</h3>
   <div id="propertytext">
   <p>Our accounts office provides monthly reports on property maintenance/repairs, tenancy levels, credits and
debits brought forward, arrears among other issues.</p>
<p>Details of the name and location of property, period for which the payments were done, date and receipt
numbers are also reflected in the monthly statements</p>
<p>Our firm provides property management services as below:</p>
<ol>
<li>Rent Collection</li>
<li>Tenant Leases Reports/Property Accounts</li>
<li>Statement Receipting</li>
<li>Bills</li>
</ol>
   </div>
   </div>
</div>

<div class="row" id="consultancy">
 
  <h3 align="center">Land Consultancy</h3>
   <div id="consultancytext">
   <p>Several considerations need to be made while purchasing land or a plot for development. We walk hand in hand with our clients to ensure that they get value for money by settling on the best deals</p>

    
   </div>
</div>


<div class="row" id="agency">
 <div class="container">
  <h3 align="center" id="darkblue">Estate Agency</h3>
  <div class="col m12">
  <p>Our company undertakes letting/leasing assignments for both new and old residential and commercial
developments.</p>
<p>The company also takes instructions from clients to sell and source properties on their behalf.</p>
<p>Our business networks help us to target the best properties. We work with you towards attaining the best
prize for your property.</p>
  </div>
  </div>
</div>
<div class="row" id="staff">
 <div class="container">
  <h3 align="center">Staff</h3>
  <div class="col m12">
  <p>Sky Plus Agencies has a team of dedicated staff members headed by a director, one operations and
marketers. This team ensures provision of service with the highest degree of professionalism, honesty and
integrity.</p>

  </div>
  </div>
</div>




<div class="row" id="rangeofservices">
 <div class="col m12">
  <h3 align="center" id="darkblue">Range of Services</h3>
  <div id="rostext">
  <p>Our business portfolio covers both residential and commercial establishments across the social economic divide.</p>
  <p>Our company takes advantage of existing social media and online platforms to keep in touch with our clientele so as to maintain open channels of communication.</p>
  </div>
 </div>
</div>


<div class="row" id="propertydist">
<div class="container">
 <h3 align="center" id="darkblue">Property Distribution</h3>
 <div class="col m4 s12">
  <ul>
  <li>Makadara</li>
  <li>CBD</li>
  <li>Buruburu</li>
  <li>Kayole</li>
  <li>Ruai</li>
  <li>Ruiru</li>
  </ul>
 </div>
 <div class="col m4 s12">
    <ul>
      <li>Juja</li>
      <li>Syokimau</li>
      <li>South B</li>
      <li>South C</li>
      <li>Imara Daima</li>
      <li>Muimara Estate</li>
    </ul>
 </div>
 <div class="col m4 s12">
   <ul>
     <li>Pipeline</li>
     <li>Tassia</li>
     <li>Ruaraka</li>
     <li>Kariobangi</li>
     <li>Embakasi</li>
   </ul>
 </div>

</div><!-- end of container -->
</div>



<!-- start of footer section -->
<!-- jquery ui -->
<script type="text/javascript" src="/resources/components/jquery-ui/jquery-ui.js"></script>

<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script type="text/javascript"></script>
  
  <script type="text/javascript">
$(document).ready(function(){
    $(window).scroll(function(){
        if($(window).scrollTop() > $(window).height()){
            $("#mynav").css({"background-color":"#2E368F"});
            $("#mynav").css({"color":"#FFF"});
            

            

              
        }
        if($(window).scrollTop() < $(window).height()){
            $("#mynav").css({"background-color":"transparent"});
            $("#mynav").css({"color":"#FFF"});
           

        }
    })
})
  
$(document).ready(function(){

     $('.slider').slider();
});

</script>

 
</body>
</html>
