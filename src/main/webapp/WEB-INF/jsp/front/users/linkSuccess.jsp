<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<head>
    <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
        <jsp:param name="title" value="Sign Up"/>
    </jsp:include>
    <!-- Custom styles for this template -->
</head>

<body>

<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>

<div class="container" id="content">
 <div class="row">
 
     <div class=" col offset-m5 m6 s12 l6">
         <h3>Password Recovery.</h3>
         <p>${message}</p>
     </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->

<%--footer--%>
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<%--end of footer--%>
<!-- Placed at the end of the document so the pages load faster -->
</body>
</html>
