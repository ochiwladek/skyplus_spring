<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
        <jsp:param name="title" value="Login"/>
    </jsp:include>
    <!-- Custom styles for this template -->
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>
<div class="container" id="content">

<div class="row">
    <form class="col offset-s2 s6" id="lgform" action="/users/login" method="post">
    <c:if test="${isFailed}">

     <div class="card-panel">
      <span class="red-text text-darken-2">Your login attempt was not successful due to ${SPRING_SECURITY_LAST_EXCEPTION.message}</span>
    </div>
                
     </c:if>
     <h4 align="center">Login</h4>

      <div class="row">
        <div class="input-field col s12">
          <i class="mdi-action-account-circle prefix"></i>
          <input id="icon_prefix" type="text" class="validate" required title="Enter a valid user name" id="username" name="username">
          <label for="icon_prefix">Username</label>
        </div>
        <div class="input-field col s12">
          <i class="mdi-action-lock prefix"></i>
          <input id="icon_locl" type="password" class="validate" id="password" name="password">
          <label for="icon_lock">Password</label>
        </div>
        <div class="input-field col offset-s4 s8">
         <button class="btn waves-effect waves-light" type="submit" name="action" id="skyplusbtn">Login
          <i class="mdi-content-send right"></i>
        </button>
        </div>
      </div>
      <br>
      <div class="col offset-s4">
      <a href="/users/signupform" class="need-help">Register/SignUp </a> <br>
          <a href="/front/account/password-recovery" class="need-help">Forgot password? Click Here </a> <br>
      <a href="/front/faqs/index" class="need-help">Need help? </a>
      </div>
    </form>
      
  </div>

</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<%--footer--%>
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<%--end of footer--%>
</body>
</html>
