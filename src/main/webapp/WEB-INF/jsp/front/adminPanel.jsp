<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/head.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
    <!-- FONTAWESOME STYLES-->
    <link href="/resources/css/font-awesome.css" rel="stylesheet" />
    <%--custom css--%>
    <link href="/resources/css/custom.css" rel="stylesheet" />
<%--google fonts--%>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/adminPagesTopmenu.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="container" id="content"></div>
<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Admin Dashboard</h2>
                <h5>Welcome Jhon Deo , Love to see you back. </h5>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-red set-icon">
                    <i class="fa fa-envelope-o"></i>
                </span>
                    <div class="text-box" >
                        <p class="main-text">120 New</p>
                        <p class="text-muted">Messages</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-bars"></i>
                </span>
                    <div class="text-box" >
                        <p class="main-text">30 Tasks</p>
                        <p class="text-muted">Remaining</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue set-icon">
                    <i class="fa fa-bell-o"></i>
                </span>
                    <div class="text-box" >
                        <p class="main-text">240 New</p>
                        <p class="text-muted">Notifications</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-brown set-icon">
                    <i class="fa fa-rocket"></i>
                </span>
                    <div class="text-box" >
                        <p class="main-text">3 Orders</p>
                        <p class="text-muted">Pending</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr />
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-blue">
                    <i class="fa fa-warning"></i>
                </span>
                    <div class="text-box" >
                        <p class="main-text">52 Important Issues to Fix </p>
                        <p class="text-muted">Please fix these issues to work smooth</p>
                        <p class="text-muted">Time Left: 30 mins</p>
                        <hr />
                        <p class="text-muted">
                          <span class="text-muted color-bottom-txt"><i class="fa fa-edit"></i>
                               Lorem ipsum dolor sit amet, consectetur adipiscing elit gthn.
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit gthn.
                               </span>
                        </p>
                    </div>
                </div>
            </div>


            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="panel back-dash">
                    <i class="fa fa-dashboard fa-3x"></i><strong> &nbsp; SPEED</strong>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipiscing sit ametsit amet elit ftr. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                </div>

            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 ">
                <div class="panel ">
                    <div class="main-temp-back">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-6"> <i class="fa fa-cloud fa-3x"></i> Newyork City </div>
                                <div class="col-xs-6">
                                    <div class="text-temp"> 10° </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel panel-back noti-box">
                <span class="icon-box bg-color-green set-icon">
                    <i class="fa fa-desktop"></i>
                </span>
                    <div class="text-box" >
                        <p class="main-text">Display</p>
                        <p class="text-muted">Looking Good</p>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- /. PAGE INNER  -->
</div>

<%--end body--%>
<!-- start of footer section -->
<!-- JQUERY SCRIPTS -->
<script src="/resources/js/jquery-1.10.2.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/resources/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/resources/js/custom.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="/resources/js/morris/raphael-2.1.0.min.js"></script>

<jsp:include page="/WEB-INF/jsp/common/footer.jsp"/>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</html>
