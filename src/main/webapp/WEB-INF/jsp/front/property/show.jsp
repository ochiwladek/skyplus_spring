<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
    <jsp:param name="title" value="Skyplus"/>
  </jsp:include>

  <style type="text/css">
    body{
      background-color:#fcfcfc;
    }
  </style>
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>



<div class="row" id="detailsrow1">
  <div class="section">
    <div id="housePicHolder" class="col m12 s12" style="background: url(${property.getAsset().name});height:auto;background-size:100%;background-repeat:no-repeat">


      <h3 align="center" id="bungalowname">${property.name}</h3>
      <br>
      <%--test--%>
      <div class="row">
        <div class="col m5 s12">
          <%--<div id="houseDetailContent">--%>
          <div class="card-panel" id="card-panel">
            <h5 id="lightblue">Cost</h5>
            <p>${property.cost}</p>
            <h5 id="lightblue">For</h5>
            <p>${property.category.name()}</p>
            <h5 id="lightblue">Overview</h5>
            <div id="propdesctext">
              ${property.description}
            </div>
          </div>
          <%--</div>--%>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="section-white" id="houseDetails">
  <div class="col m12">
   <h4 align="center" id="darkblue">Sections</h4>



        <c:choose>
          <c:when test="${empty property.sections}">

              <h4 align="center">There Are no sections available</h4>


          </c:when>
          <c:otherwise>
           <div class="row">
            <c:forEach items="${property.sections}" var="section">
                       <div class="col m4 s12">
                           <div class="card small">
                            <div class="card-image" id="secimage">
                               <img src="${section.asset.name}">
                               <span class="card-title">${section.name}</span>

                            </div>
                          </div>
                      </div>
            </c:forEach>
            </div>
          </c:otherwise>
        </c:choose>

    </div><!-- end of col m12 -->
  </div>
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
<!-- jquery ui -->
<script type="text/javascript" src="/resources/components/jquery-ui/jquery-ui.js"></script>
</html>
