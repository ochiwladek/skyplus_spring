<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
    <jsp:param name="title" value="Skyplus"/>
  </jsp:include>

  <style type="text/css">
  #maincategories{
    padding-top: 80px;
    padding-bottom: 80px;
  }
 
  </style>

<!-- jquery ui -->
<script type="text/javascript" src="/resources/components/jquery-ui/jquery-ui.js"></script>

</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<%--main content--%>
<div class="row" id="maincategories">
<h4 id="darkblue" align="center">Property Availble  ${propertyPage.content.size()}</h4>
  <div class="divider" id="landliner"></div>
  <div class="row" id="landlist">

  <div class="container">
    
    <div class="col  m12 s12">
        <c:if test="${isFailed}">

            <div class="card-panel">
                <span class="red-text text-darken-2">Your inquiry failed. Form error</span>
                <form:form modelAttribute="landInquiry">
                    <p>
                        Email &nbsp;<form:errors path="email" />
                    </p>
                    <p>
                        Title &nbsp;<form:errors path="title" />
                    </p>
                    <p>
                        detail &nbsp;<form:errors path="detail" />
                    </p>
                </form:form>
            </div>

        </c:if>
        <c:if test="${isPosted}">
            <div class="card-panel">
                <span class="red-text text-darken-2">Thank you for your inquiry. We will get back to soonest</span>
            </div>
        </c:if>
    
     <c:forEach items="${propertyPage.content}" var="property">
         <div class="row">
     <div class="card-panel">
    <div class="row">
     <h5 align="center" id="lightblue">${property.name}   ${propertyPage.content.size()}</h5>
     <div class="col m6">
      <div class="card small">
      <div class="card-content">
      <div class="col m12">
          <!-- end of slider -->
          <div class="card-image waves-effect waves-block waves-light">
              <img class="activator" src="${property.asset.name}" style="width: auto ; height: auto">
          </div>
       </div><!-- end of col m 12 -->
       </div><!-- end of card-content -->
       </div><!-- end of card small -->
        </div><!-- end ofcol m6 -->
         <div class="col m6">
            <div class="row">
              <h5 align="left" id="darkblue">Description</h5>
              <div class="divider" id="leftliner"></div>
              <div id="landdesc">
                  <p>${property.description}</p>
              </div><!-- end of landdesc -->

            </div><!-- end of description row -->
            <div class="row">
               <div class="col m12">
                 <h5>Sh ${property.cost}</h5>
               </div>

                <div class="col m12">
                   <div id="landbuttons">
                      <!-- Modal Trigger -->
                        <a class="waves-effect waves-light btn modal-trigger" id="darkbluebtn" href="#${property.id}">Enquire</a>
                        <!-- end of modal -->
                   </div>
                    <!-- end of land buttons -->
                </div><!-- end of col m12 -->

           </div><!-- end of row -->
        </div><!-- end of col m6 -->


      </div><!-- end of row -->
      </div><!-- end of card panel -->
             </div>
      </c:forEach>
     </div>
     </div><!-- end of container -->
     </div><!-- end of row -->
    
   
    </div>
        </div>
      </div>
  </div>
  <div class="section-white">
    <jsp:include page="/WEB-INF/jsp/common/pagination.jsp">
      <jsp:param name="paginatedRecord" value="propertyPage"/>
      <jsp:param name="url" value="/${pagenatedUrl}"/>
    </jsp:include>
  </div>
</div>
<%--end main content--%>
<%--end body--%>
<!-- start of footer section -->
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>

<script type="text/javascript">
 $(document).ready(function(){
     $('.slider').slider({full_width: true});

     
    }); 

</script>

 
</body>
</html>
