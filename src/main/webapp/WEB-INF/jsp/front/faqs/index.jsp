<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <jsp:include page="/WEB-INF/jsp/common/head2.jsp">
    <jsp:param name="title" value="Skyplus"/>
  </jsp:include>
  <style type="text/css">
    #main{
      height:300px;
      width:300px;
      margin-left: 15px;
      margin-right: 15px;
      position: relative;
      overflow: hidden;

    }

    #mainh1{
      color:#2D368F;
    }

    #myimage img{

      height:300px;
      width:300px;
      transition:0.5s;


    }

    .col-md-3{
      margin-top: 10px;
    }




    #shade{
      height:400px;
      padding:0px 0px 0px 0px;
      margin: 0px 0px 0px 0px;
      transition:0.3s;
    }

    #shade:hover{

      background-color:rgba(0,0,0,0.8);

    }

    #mydescription{
      width: 70%;
      background-color: transparent;
      color:#FFF;
      transform:translateY(-200px);
    }

    #mybuttons{
      text-align: center;



      transition:0.3s;

    }

    #main:hover #mybuttons{

      transform:translateY(-200px);

    }



    #main:hover img{
      transform:scale(1.2);
    }

    #viewbtn{
      padding: 10px 10px 10px 10px;
      border-radius:0px;
      background-color:rgba(255,255,255.0.8);
      color:#FFF;
    }
  </style>
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.3";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<div class="row" id="apartments">
<div class="container">
  <h4 align="center" id="darkblue"> Frequently Asked Questions(FAQS)</h4>
  <div class="col m4 s12">
    <c:forEach items="${faqsList}" var="faq">
      <div class="row">
        <li><a href="/front/faqs/read-more?faqId=${faq.id}">${faq.question}</a></li>
      </div>
    </c:forEach>
  </div>
  <div class="col m6 s12">
    <c:choose>
      <c:when test="${selectedFaq == null}">
        <h5>Click on a question on the left to read more</h5>
      </c:when>
      <c:otherwise>
        <h5>${selectedFaq.question}</h5>
        <p>${selectedFaq.answer}</p>
        <p>For more information, <a href="/front/contactUs">contact us</a></p>
      </c:otherwise>
    </c:choose>
  </div>
  </div><!-- end of container -->

</div>

<%--end body--%>
<!-- start of footer section -->
<jsp:include page="/WEB-INF/jsp/common/footer2.jsp"/>
</body>
</html>
