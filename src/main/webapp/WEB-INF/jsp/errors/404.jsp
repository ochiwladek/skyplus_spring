<%--
  Created by IntelliJ IDEA.
  User: wladek
  Date: 6/29/15
  Time: 4:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Skyplus</title>
  <jsp:include page="/WEB-INF/jsp/common/head.jsp">
    <jsp:param name="" value="Skyplus"/>
  </jsp:include>

</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/topmenu.jsp"/>


<!-- ================================================== -->
<!-- Header -->
<div class="error404">

  <div class="container-fluid">

    <div class="row">
      <div class="col-sm-12">


        <div class="error-message" id="header">

          <h1 id="mainheader">ERROR 404</h1>
          <h1 >OOps..well thats weird.</h1>

          <ul class="list-inline intro-social-buttons">

          </ul>


        </div>
      </div>

    </div>

  </div>
  <!-- /.container -->

</div>
<!-- /.intro-header -->
<!-- ================================================== -->



</body>
</html>
