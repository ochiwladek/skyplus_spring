<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Land Registration</h3>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <c:choose>
            <c:when test="${success == true}">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <span class="sr-only">Success:</span>
                        ${message}
                </div>
            </c:when>
        </c:choose>

        <form:form class="form-horizontal" modelAttribute="land" method="post" action="/admin/land/form/${action}"
                   enctype="multipart/form-data">
            <div class="form-group">

                <label for="name" class="control-label col-xs-2">Name</label>

                <div class="col-xs-7">

                    <form:input path="name" type="text" id="name" class="form-control" placeholder="Land name" />
                        <span class="help-inline">
                            <form:errors path="name" />
                        </span>

                </div>

            </div>
            <div class="form-group">

                <label for="size" class="control-label col-xs-2">Size</label>

                <div class="col-xs-7">

                    <form:input path="size" type="text" id="size" class="form-control" placeholder="Size of land" />
                        <span class="help-inline">
                            <form:errors path="size" />
                        </span>

                </div>

            </div>
            <div class="form-group">

                <label for="cost" class="control-label col-xs-2">Cost</label>

                <div class="col-xs-3">

                    <form:input path="cost" type="text" id="cost" class="form-control" placeholder="Size of land" />
                        <span class="help-inline">
                            <form:errors path="cost" />
                            <form:errors />
                        </span>

                </div>

                <label for="currencyId" class="control-label col-xs-1">Currency</label>

                <div class="col-xs-3">
                    <form:select path="currencyId" id="currencyId" class="form-control" required="required">
                        <form:option value=""></form:option>
                        <c:forEach items="${currencyList}" var="currency">
                            <form:option value="${currency.id}">${currency.name}</form:option>
                        </c:forEach>
                    </form:select>
                </div>

            </div>
        <div class="form-group">

            <label for="townId" class="control-label col-xs-2">Town</label>

            <div class="col-xs-3">
                <form:select path="townId" id="townId" class="form-control" required="required">
                    <form:option value=""></form:option>
                    <c:forEach items="${townList}" var="town">
                        <form:option value="${town.id}">${town.name}</form:option>
                    </c:forEach>
                </form:select>
            </div>
            </div>
            <div class="form-group">

                <label for="description" class="control-label col-xs-2">Description</label>

                <div class="col-xs-7">
                    <form:textarea path="description" id="description" placeholder="A brief description" class="form-control" />
                    <span class="help-inline">
                            <form:errors path="description" />
                        </span>
                </div>

            </div>
            <div class="form-group">

                <label for="file1" class="control-label col-xs-2">Photo 1</label>

                <div class="col-xs-7">

                    <input type="file" name="file" id="file1" required="required">

                </div>

            </div>
            <div class="form-group">

                <label for="file2" class="control-label col-xs-2">Photo 2</label>

                <div class="col-xs-7">

                    <input type="file" name="file" id="file2" required="required">

                </div>

            </div>
            <div class="form-group">

                <label for="file3" class="control-label col-xs-2">Photo 3</label>

                <div class="col-xs-7">

                    <input type="file" name="file" id="file3" required="required">

                </div>

            </div>
            <div class="form-group">

                <div class="col-xs-offset-2 col-xs-10">

                    <button type="submit" class="btn btn-primary">Submit</button>

                </div>

            </div>
        </form:form>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-plus"></span>
        <a href="/admin/land/index" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->
<script>
    $('.fileupload').fileupload();
</script>
<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
