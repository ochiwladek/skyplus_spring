<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >
                    <c:choose>
                        <c:when test="${success == true}">
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span class="sr-only">Success:</span>
                                    ${message}
                            </div>
                        </c:when>
                    </c:choose>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Inquiry for ${landInquiry.land.name}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class=" col-md-9 col-lg-9 col-sm-12">
                                    <p><h3>From :</h3> ${landInquiry.email}</p>
                                    <p><h3>Title :</h3> ${landInquiry.title}</p>
                                    <p><h3>Detail : </h3> ${landInquiry.detail}</p>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <table class="table table-user-information">
                                                <tbody>
                                                <tr>
                                                    <td>Name:</td>
                                                    <td>${landInquiry.land.name}</td>
                                                </tr>
                                                <tr>
                                                    <td>Size:</td>
                                                    <td>${landInquiry.land.size}</td>
                                                </tr>
                                                <tr>
                                                    <td>Town</td>
                                                    <td>${landInquiry.land.town.name}</td>
                                                </tr>

                                                <tr>
                                                <tr>
                                                    <td>Cost</td>
                                                    <td>${landInquiry.land.currency.symbol}&nbsp;&nbsp;${land.cost}</td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>${landInquiry.land.state}</td>
                                                </tr>
                                                <td>Description</td>
                                                <td><P>${landInquiry.land.description}</P>
                                                </td>

                                                </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="panel-footer">
                                            <a data-original-title="Reply" data-toggle="modal" data-target="#formModal" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="box-footer clearfix">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <a href="/admin/land-inquiry/home" class="btn btn-small">Back</a>
        </div>
    </div>
</div>
<%--end body--%>
<%--begin modal--%>
<div class="modal fade bs-example-modal-lg" id="formModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">${sectionModal.name}</h4>
            </div>
            <div class="modal-body">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >
                    <c:choose>
                        <c:when test="${success == true}">
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span class="sr-only">Sending....:</span>
                                    ${message}
                            </div>
                        </c:when>
                    </c:choose>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Reply for ${landInquiry.land.name} inquiry</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class=" col-md-9 col-lg-9 col-sm-12">
                                    <form:form class="form-horizontal" modelAttribute="reply" method="post" action="/admin/land-inquiry/reply-mail"
                                               enctype="multipart/form-data">
                                        <div class="form-group">

                                            <label for="email" class="control-label col-xs-2">Subject</label>

                                            <div class="col-xs-7">
                                                <p><h4>${landInquiry.title}</h4></p>
                                            </div>

                                        </div>
                                        <div class="form-group">

                                            <label for="email" class="control-label col-xs-2">Description</label>

                                            <div class="col-xs-7">
                                                <form:textarea path="email" id="email" placeholder="Content here" required="required" class="form-control" />
                                                <form:input path="inquiryId" type="hidden" value="${landInquiry.id}" />
                                                <form:input path="toEmail" type="hidden" value="${landInquiry.email}" />
                                                <form:input path="subject" type="hidden" value="${landInquiry.title}" />
                    <span class="help-inline">
                            <form:errors path="email" />
                        </span>
                                            </div>

                                        </div>
                                        <div class="form-group">

                                            <div class="col-xs-offset-2 col-xs-10">

                                                <button type="submit" class="btn btn-primary">Reply</button>

                                            </div>

                                        </div>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--end modal--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
