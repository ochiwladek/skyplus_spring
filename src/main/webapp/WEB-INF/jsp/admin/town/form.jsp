<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Town Registration</h3>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <c:choose>
            <c:when test="${success == true}">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <span class="sr-only">Success:</span>
                        ${message}
                </div>
            </c:when>
        </c:choose>

        <form:form class="form-horizontal" modelAttribute="town" method="post" action="/admin/town/form"
                   enctype="multipart/form-data">

                <div class="form-group">

                    <label for="name" class="control-label col-xs-2">Town Name</label>

                    <div class="col-xs-7">

                        <form:input path="name" type="text" id="name" required="required" pattern="[A-Za-z]+" title="Must begin with a capital letter" class="form-control" placeholder="Town name" />
                        <span class="help-inline">
                            <form:errors path="name" />
                        </span>
                        <form:input path="townId" type="hidden" />
                        <form:input path="action" type="hidden" />

                    </div>

                </div>

                <div class="form-group">

                    <div class="col-xs-offset-2 col-xs-10">

                        <button type="submit" class="btn btn-primary">Submit</button>

                    </div>

                </div>
        </form:form>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-plus"></span>
        <a href="/admin/town/index" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->
<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
