<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <div class="box-tools">
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >


                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">${property.name}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 " align="center">
                                    <img alt="User Pic" src="${property.asset.name}" height="100%" width="100%" class="img-square"> </div>
                                <div class=" col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td>Property Name:</td>
                                            <td>${property.name}</td>
                                        </tr>
                                        <tr>
                                        <tr>
                                            <td>Type:</td>
                                            <td>${property.type}</td>
                                        </tr>
                                        <tr>
                                            <td>Cost:</td>
                                            <td>${property.cost}</td>
                                        </tr>
                                        <tr>
                                            <td>Category:</td>
                                            <td>${property.category.name()}</td>
                                        </tr>
                                        <tr>
                                            <td>Subcategory:</td>
                                            <td>${property.subcategory}</td>
                                        </tr>
                                        <tr>
                                            <td>Description:</td>
                                            <td>${property.description}</td>
                                        </tr>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer clearfix">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <a href="/admin/property/" class="btn btn-small">Back</a>
        </div>
        <div class="box-body">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >


                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Sections</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="table-responsive">
                                        <c:choose>
                                            <c:when test="${empty property.sections}">
                                                <div class="alert alert-info">
                                                    <strong>Empty!</strong> No sections to display.
                                                    <p><a href="/admin/section/${property.id}/form">Create New</a> </p>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <p><a href="/admin/section/${property.id}/form">Add section</a> </p>
                                                <c:forEach items="${property.sections}" var="section">
                                                    <div class="col-sm-4">
                                                        <div class="panel">
                                                            <div class="panel-body">
                                                                <h4>${section.name}</h4>
                                                                <p>
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${section.id}">
                                                                        More Detail
                                                                    </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <a href="/admin/property/" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<%--begin modal--%>
<c:forEach items="${property.sections}" var="sectionModal">
    <div class="modal fade bs-example-modal-lg" id="${sectionModal.id}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">${sectionModal.name}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                            <div class="col-sm-4">
                                    <img src="${sectionModal.asset.name}" />
                            </div>
                    </div>
                    <div class="row">
                        <p>${sectionModal.description}</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</c:forEach>
<%--end modal--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>

<script>
    $(document).ready(function(){
        $('#submit').click(function(){
            var id = $('#idnumber').val();

            alert(id);
        });
    });
</script>
</html>
