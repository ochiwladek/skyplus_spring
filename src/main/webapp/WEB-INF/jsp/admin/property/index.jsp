<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h2 class="box-title">Property Summary</h2>
        <div class="input-group">
            <div class="input-group-btn">
                ADD
                <a href="/admin/property/form" class="btn btn-sm btn-default"><i class="fa fa-plus-square-o"></i></a>
            </div>
        </div>
        <div class="box-tools">
            <div class="input-group">
                <div class="input-group-btn">
                    <a href="/admin/property/form" class="btn btn-sm btn-default"><i class="fa fa-plus-square-o"></i></a>
                </div>
            </div>
            <div class="input-group">
                <input type="text" id="table_search" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search by block"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Cover photo</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Category</th>
                    <th>Subcategory</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <c:forEach items="${propertyPage.content}" var="property">
                    <tbody>
                    <td><img src="${property.asset.name}" style="width: 30%; height: 30%"></td>
                    <td>${property.name}</td>
                    <td>${property.type}</td>
                    <td>${property.category}</td>
                    <td>${property.subcategory}</td>
                    <td>
                       <a href="/admin/property/show/${property.id}">View</a>
                    </td>
                    <td>
                        <a href="/admin/property/edit/${property.id}">Edit</a>
                    </td>
                    <td>
                        <a href="/admin/property/delete/${property.id}">Delete</a>
                    </td>
                    </tbody>
                </c:forEach>
            </table>
        </div>
    </div>
    <div class="box-footer clearfix">
        <jsp:include page="/WEB-INF/jsp/common/pagination.jsp">
            <jsp:param name="paginatedRecord" value="propertyPage"/>
            <jsp:param name="url" value="${pagenatedUrl}"/>
        </jsp:include>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
