<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Property Registration</h3>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <c:choose>
            <c:when test="${success == true}">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <span class="sr-only">Success:</span>
                        ${message}
                </div>
            </c:when>
        </c:choose>

        <form:form class="form-horizontal" modelAttribute="property" method="post" action="/admin/property/form/${action}"
                   enctype="multipart/form-data">
            <div class="form-group">

                <c:choose>
                    <c:when test="${property.asset.name == null}">
                        <label for="file" class="control-label col-xs-2">Cover Photo</label>

                        <div class="col-xs-7">

                            <input type="file" id="file" name="file" class="form-control" required="required">
                        </div>
                    </c:when>
                    <c:otherwise>
                        <label for="file" class="control-label col-xs-2">Cover Photo</label>

                        <div class="col-xs-7">

                            <img src="${property.asset.name}" height="20%" width="25%">
                            <input type="file" id="file" name="file" class="form-control" required="required">
                        </div>
                    </c:otherwise>
                </c:choose>

            </div>
            <div class="form-group">

                <label for="name" class="control-label col-xs-2">Property Name</label>

                <div class="col-xs-7">

                    <form:input path="name" type="text" id="number" class="form-control" required="required" placeholder="Name" />
                        <span class="help-inline">
                            <form:errors path="name" />
                        </span>

                </div>

            </div>

            <div class="form-group">

                <label for="description" class="control-label col-xs-2">Description</label>

                <div class="col-xs-7">
                    <form:textarea path="description" id="description" placeholder="A brief description" required="required" class="form-control" />
                    <form:input path="id" type="hidden" />

                    <span class="help-inline">
                            <form:errors path="description" />
                        </span>
                </div>

            </div>

            <div class="form-group">

                <label for="description" class="control-label col-xs-2">Cost</label>

                <div class="col-xs-7">
                    <form:input path="cost" id="cost" placeholder="Cost" class="form-control" />

                    <span class="help-inline">
                            <form:errors path="cost" />
                        </span>
                </div>

            </div>

            <div class="form-group">

                <label for="type" class="control-label col-xs-2">Type</label>

                <div class="col-xs-2">

                    <select id="type" name="type" class="form-control" required="required" title="Select Category">
                        <option></option>
                        <option value="apartment">Apartment</option>
                        <option value="bungalow">Bungalow</option>
                        <option value="maisonette">Maisonette</option>
                        <option value="land">Land</option>
                    </select>

                </div>

                <label for="categoryId" class="control-label col-xs-1">Category</label>

                <div class="col-xs-2">

                    <select id="categoryId" name="categoryId" class="form-control" required="required" title="Select Category">
                        <option></option>
                        <option value="sale">For Sale</option>
                        <option value="let">To Let</option>
                    </select>

                </div>

                <label for="subcategory" class="control-label col-xs-1">Sub Category</label>

                <div class="col-xs-2">

                    <select id="subcategory" name="subcategory" class="form-control" required="required" title="Select Subcategory">
                        <option></option>
                        <option value="commercial">Commercial</option>
                        <option value="furnished">Furnished</option>
                        <option value="unfurnished">Un-furnished</option>
                        <option value="residential">Residential</option>
                        <option value="plot">Plot</option>
                    </select>
                </div>

            </div>

                <div class="form-group">

                    <div class="col-xs-offset-2 col-xs-10">

                        <button type="submit" class="btn btn-primary">Submit</button>

                    </div>

                </div>
        </form:form>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->
<script>
    $('.fileupload').fileupload();
</script>
<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
