<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <div class="box-tools">
            <%--<div class="input-group">--%>
                <%--<input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>--%>
                <%--<div class="input-group-btn">--%>
                    <%--<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>--%>
                <%--</div>--%>
            <%--</div>--%>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >
                    <c:choose>
                        <c:when test="${success == true}">
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span class="sr-only">Success:</span>
                                    ${message}
                            </div>
                        </c:when>
                    </c:choose>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">${apartment.name}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td>Apartment Name:</td>
                                            <td>${apartment.name}</td>
                                        </tr>
                                        <tr>
                                            <td>Apartment Number:</td>
                                            <td>${apartment.number}</td>
                                        </tr>
                                        <tr>
                                            <td>Block</td>
                                            <td><a href="/admin/landlord/show/${apartmentBlock.plot.landlord.id}">
                                            ${apartment.apartmentBlock.name}
                                            </a>
                                            </td>
                                        </tr>

                                        <tr>
                                        <tr>
                                            <td>Bedrooms</td>
                                            <td>${apartment.bedroom}</td>
                                        </tr>
                                        <tr>
                                            <td>Cost:</td>
                                            <td>${apartment.currency.symbol}&nbsp;&nbsp;${apartment.cost}</td>
                                        </tr>
                                        <tr>
                                            <td>Category:</td>
                                            <td>${apartment.apartmentCategory.name}</td>
                                        </tr>
                                        <tr>
                                            <td>Sub category:</td>
                                            <td>${apartment.subCategory.name}</td>
                                        </tr>
                                        <tr>
                                            <td>State:</td>
                                            <td>${apartment.state.name()}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <c:choose>
                                                    <c:when test="${apartment.state == 'VACANT'}">
                                                        <button type="button" class="btn btn-small" data-toggle="modal" data-target="#status">
                                                            Rent Apartment
                                                        </button>
                                                    </c:when>
                                                    <c:when test="${apartment.state == 'AVAILABLE'}">
                                                        <button type="button" class="btn btn-small" data-toggle="modal" data-target="#sell">
                                                            Sell Apartment
                                                        </button>
                                                    </c:when>
                                                </c:choose>
                                            </td>
                                        </tr>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="box-footer clearfix">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <a href="/admin/apartment/index" class="btn btn-small">Back</a>
        </div>
        <div class="box-body">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >


                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Apartment Sections</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="table-responsive">
                                        <c:choose>
                                            <c:when test="${empty apartment.apartmentSections}">
                                                <div class="alert alert-info">
                                                    <strong>Empty!</strong> No Apartment Sections to display.
                                                    <p><a href="/admin/apartment/section/form?id=${apartment.id}">Add section</a> </p>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <p><a href="/admin/apartment/section/form?id=${apartment.id}">Add section</a> </p>
                                                <c:forEach items="${apartment.apartmentSections}" var="section">
                                                    <div class="col-sm-4">
                                                        <div class="panel">
                                                            <div class="panel-body">
                                                                    <h4>${section.name}</h4>
                                                                <p>
                                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${section.id}">
                                                                        More Detail
                                                                    </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <a href="/admin/apartment/index" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<%--begin modal--%>
<c:forEach items="${apartment.apartmentSections}" var="sectionModal">
    <div class="modal fade bs-example-modal-lg" id="${sectionModal.id}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">${sectionModal.name}</h4>
                </div>
                <div class="modal-body">
                    <div id="myCarousel" class="carousel slide">
                        <ol class="carousel-indicators">
                            <c:forEach items="${sectionModal.assets}" var="indicator" varStatus="i">
                                <c:choose>
                                    <c:when test="${i.index == 0}">
                                        <li data-target="#myCarousel" data-slide-to="${indicator.id}" class="active"></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li data-target="#myCarousel" data-slide-to="${indicator.id}"></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </ol>
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                            <c:forEach items="${sectionModal.assets}" var="inner" varStatus="a">
                                <c:choose>
                                    <c:when test="${a.index == 0}">
                                        <div class="active item">
                                            <img src="${inner.name}">
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="item">
                                            <img src="${inner.name}">
                                        </div>
                                    </c:otherwise>
                                </c:choose>

                            </c:forEach>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev"><i class="fa fa-arrow-left fa-3x"></i></a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="fa fa-arrow-right fa-3x"></i></a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</c:forEach>

<div class="modal fade bs-example-modal-lg" id="status" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Rent Apartment</h4>
            </div>
            <div class="modal-body">
                <form:form class="form-horizontal" modelAttribute="status" method="post" action="/admin/apartment/status/${apartment.id}"
                           enctype="multipart/form-data">
                    <div class="form-group">

                        <label for="idNumber" class="control-label col-xs-2">Tenant Id Number</label>

                        <div class="col-xs-7">

                            <form:input path="idNumber" type="text" id="idNumber" required="required" class="form-control" placeholder="Id/passport Number" />
                          <span class="help-inline">
                            <form:errors path="idNumber" />
                          </span>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="status" class="control-label col-xs-2">Action</label>

                        <div class="col-xs-7">
                            <select id="status" name="status" class="form-control" required="required">
                                <option></option>
                                <option value="Rent">Rent</option>
                                <option value="Vacate">Vacate</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-offset-2 col-xs-10">

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </div>

                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<%--Selling modal--%>
<div class="modal fade bs-example-modal-lg" id="sell" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Buyer Details</h4>
            </div>
            <div class="modal-body">
                <form:form class="form-horizontal" modelAttribute="apartmentBuyer" method="post" action="/admin/apartment/sell/${apartment.id}"
                           enctype="multipart/form-data">
                    <div class="form-group">

                        <label for="fullName" class="control-label col-xs-2">Full Names</label>

                        <div class="col-xs-7">

                            <form:input path="fullName" type="text" id="fullName" required="required" class="form-control" placeholder="Full names" />
                          <span class="help-inline">
                            <form:errors path="fullName" />
                          </span>
                        </div>
                    </div>
                    <div class="form-group">

                        <label for="idNumber" class="control-label col-xs-2">Id Number</label>

                        <div class="col-xs-7">

                            <form:input path="idNumber" type="text" id="idNumber" required="required" class="form-control" placeholder="Id/passport Number" />
                          <span class="help-inline">
                            <form:errors path="idNumber" />

                              <form:input path="apartmentId" value="${apartment.id}" type="hidden" />
                          </span>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-xs-offset-2 col-xs-10">

                            <button type="submit" class="btn btn-primary">Submit</button>

                        </div>

                    </div>
                </form:form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<%--end selling modal--%>

<%--end modal--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>

<script>
    $(document).ready(function(){
        $('#submit').click(function(){
            var id = $('#idnumber').val();

            alert(id);
        });
    });
</script>
</html>
