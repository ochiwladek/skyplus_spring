<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h2 class="box-title">Land Summary</h2>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" id="table_search" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search by number"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >
                    <c:choose>
                        <c:when test="${success == true}">
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                <span class="sr-only">Success:</span>
                                    ${message}
                            </div>
                        </c:when>
                    </c:choose>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">Total ${apartmentPage.content.size()}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class=" col-md-9 col-lg-9 col-sm-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Pic</th>
                                                <th>Number</th>
                                                <th>Bedrooms</th>
                                                <th>Status</th>
                                                <th>Category</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <c:forEach items="${apartmentPage.content}" var="apartment">
                                                <tbody>
                                                <td>
                                                    <img src="${apartment.asset.name}" height="55px" width="45px"
                                                         alt="Profile photo">
                                                </td>
                                                <td>${apartment.number}</td>
                                                <td>${apartment.bedroom}</td>
                                                <td>${apartment.state}</td>
                                                <td>${apartment.apartmentCategory.name}</td>
                                                <td>
                                                    <a href="/admin/apartment/show/${apartment.id}">View</a>
                                                </td>
                                                <td>
                                                    <a href="/admin/apartment/show/delete/${apartment.id}">Delete</a>
                                                </td>
                                                </tbody>
                                            </c:forEach>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3 col-sm-12">
                                    <div class="panel">
                                        <div class="panel-heading">
                                            <h4>Filter by</h4>
                                        </div>
                                        <div class="panel-body">
                                            <form:form class="form-horizontal" modelAttribute="filter" method="get" action="/admin/apartment/filter"
                                                       enctype="multipart/form-data">
                                                <div class="form-group">

                                                    <label for="status" class="control-label col-xs-offset-0 col-xs-2">Status</label>

                                                    <div class="col-xs-offset-3 col-xs-7">
                                                        <select name="status" id="status" class="form-control" required="required">
                                                            <option selected value="all">All</option>
                                                            <option value="vacant">Vacant</option>
                                                            <option value="occupied">Occupied</option>
                                                            <option value="available">Available</option>
                                                            <option value="sold">Sold</option>
                                                            <option value="repair">On repair</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="form-group">

                                                    <label for="status" class="control-label col-xs-offset-0 col-xs-2">Category</label>

                                                    <div class="col-xs-offset-3 col-xs-7">
                                                        <select name="category" id="status" class="form-control" required="required">
                                                            <option selected value="all">All</option>
                                                            <option value="Sale">Sale</option>
                                                            <option value="Rent">Rent</option>
                                                            <option value="Give-and-go">Give and go</option>
                                                        </select>
                                                    </div>

                                                </div>
                                                <div class="form-group">

                                                    <div class="col-xs-offset-0 col-xs-10">

                                                        <button type="submit" class="btn btn-primary">Submit</button>

                                                    </div>

                                                </div>
                                            </form:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer clearfix">
        <jsp:include page="/WEB-INF/jsp/common/pagination.jsp">
            <jsp:param name="paginatedRecord" value="apartmentPage"/>
            <jsp:param name="url" value="${pagenatedUrl}"/>
        </jsp:include>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
