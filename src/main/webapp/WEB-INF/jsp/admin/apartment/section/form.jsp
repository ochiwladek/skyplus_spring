<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Apartment Section Form</h3>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <form:form class="form-horizontal" modelAttribute="section" method="post" action="/admin/apartment/section/form/${action}"
                   enctype="multipart/form-data">
            <div class="form-group">

                <label for="name" class="control-label col-xs-2">Name</label>

                <div class="col-xs-7">

                    <form:input path="name" type="text" id="name" class="form-control" required="required" title="Must start with a capital letter" placeholder="Apartment name" />
                        <span class="help-inline">
                            <form:errors path="name" />
                        </span>

                </div>

            </div>
            <div class="form-group">

                <label for="description" class="control-label col-xs-2">Description</label>

                <div class="col-xs-7">
                    <form:textarea path="description" id="description" placeholder="A brief description" class="form-control" required="required"/>
                    <form:input path="apartmentId" value="${apartmentId}" type="hidden" />

                    <span class="help-inline">
                            <form:errors path="description" />
                        </span>
                </div>

            </div>
            <div class="form-group">

                <label for="file1" class="control-label col-xs-2">Photo 1</label>

                <div class="col-xs-7">

                    <input type="file" name="file" id="file1" required="required">

                </div>

            </div>
            <div class="form-group">

                <label for="file2" class="control-label col-xs-2">Photo 2</label>

                <div class="col-xs-7">

                    <input type="file" name="file" id="file2" required="required">

                </div>

            </div>
            <div class="form-group">

                <label for="file3" class="control-label col-xs-2">Photo 3</label>

                <div class="col-xs-7">

                    <input type="file" name="file" id="file3" required="required">

                </div>

            </div>
                <div class="form-group">

                    <div class="col-xs-offset-2 col-xs-10">

                        <button type="submit" class="btn btn-primary">Submit</button>

                    </div>

                </div>
        </form:form>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-plus"></span>
        <a href="#" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->
<script>
    $('.fileupload').fileupload();
</script>
<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
