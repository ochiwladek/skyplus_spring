<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Apartment Registration</h3>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <c:choose>
            <c:when test="${success == true}">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    <span class="sr-only">Success:</span>
                        ${message}
                </div>
            </c:when>
        </c:choose>

        <form:form class="form-horizontal" modelAttribute="apartment" method="post" action="/admin/apartment/form/${action}"
                   enctype="multipart/form-data">
            <div class="form-group">

                <label for="name" class="control-label col-xs-2">Cover Photo</label>

                <div class="col-xs-7">

                    <input type="file" id="file" name="file" class="form-control" required="required">
                </div>

            </div>
            <div class="form-group">

                <label for="name" class="control-label col-xs-2">Name</label>

                <div class="col-xs-7">

                    <form:input path="name" type="text" id="name" class="form-control" placeholder="Apartment name" />
                        <span class="help-inline">
                            <form:errors path="name" />
                        </span>

                </div>

            </div>
                <div class="form-group">

                    <label for="number" class="control-label col-xs-2">Number</label>

                    <div class="col-xs-7">

                        <form:input path="number" type="text" id="number" class="form-control" placeholder="Apartment Number"
                                    required="required" />
                        <span class="help-inline">
                            <form:errors path="number" />
                        </span>

                    </div>

                </div>

                <div class="form-group">

                    <label for="bedroom" class="control-label col-xs-2">Number of bedrooms</label>

                    <div class="col-xs-7">

                        <form:input path="bedroom" type="text" id="bedroom" placeholder="Number of bedrooms" class="form-control" />

                        <span class="help-inline">
                            <form:errors path="bedroom" />
                        </span>
                    </div>

                </div>

            <div class="form-group">

                <label for="cost" class="control-label col-xs-2">Cost</label>

                <div class="col-xs-3">

                    <form:input path="cost" type="text" id="cost" class="form-control" placeholder="Cost of apartment" />
                        <span class="help-inline">
                            <form:errors path="cost" />
                            <form:errors />
                        </span>

                </div>

                <label for="currencyId" class="control-label col-xs-1">Currency</label>

                <div class="col-xs-3">
                    <form:select path="currencyId" id="currencyId" class="form-control" required="required">
                        <form:option value=""></form:option>
                        <c:forEach items="${currencyList}" var="currency">
                            <form:option value="${currency.id}">${currency.name}</form:option>
                        </c:forEach>
                    </form:select>
                </div>

            </div>

            <div class="form-group">

                <label for="description" class="control-label col-xs-2">Description</label>

                <div class="col-xs-7">
                    <form:textarea path="description" id="description" placeholder="A brief description" class="form-control" />
                    <form:input path="apartmentBlockId" value="${apartmentBlockId}" type="hidden" />

                    <span class="help-inline">
                            <form:errors path="description" />
                        </span>
                </div>

            </div>

            <div class="form-group">

                <label for="apartmentCategoryId" class="control-label col-xs-2">Main Category</label>

                <div class="col-xs-3">
                    <select id="apartmentCategoryId" name="apartmentCategoryId" class="form-control">
                        <option value=""></option>
                        <c:forEach items="${apartmentCategories}" var="category">
                            <option value="${category.id}">${category.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <label for="subcategoryId" class="control-label col-xs-1">Subcategory</label>

                <div class="col-xs-3">
                    <select id="subcategoryId" name="subcategoryId" class="form-control">
                        <option value=""></option>
                        <c:forEach items="${subCategories}" var="subCategory">
                            <option value="${subCategory.id}">${subCategory.name}</option>
                        </c:forEach>
                    </select>
                </div>

            </div>
                <div class="form-group">

                    <div class="col-xs-offset-2 col-xs-10">

                        <button type="submit" class="btn btn-primary">Submit</button>

                    </div>

                </div>
        </form:form>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-plus"></span>
        <a href="#" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->
<script>
    $('.fileupload').fileupload();
</script>
<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
