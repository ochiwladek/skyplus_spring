<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <div class="box-tools">
            <%--<div class="input-group">--%>
                <%--<input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>--%>
                <%--<div class="input-group-btn">--%>
                    <%--<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>--%>
                <%--</div>--%>
            <%--</div>--%>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >


                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">${apartmentBlock.name}</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 " align="center">
                                    <img alt="Home pic" src="${apartmentBlock.asset.name}" height="100%" width="100%" class="img-square"> </div>
                                <div class=" col-md-9 col-lg-9 ">
                                    <table class="table table-user-information">
                                        <tbody>
                                        <tr>
                                            <td>Block Name:</td>
                                            <td>${apartmentBlock.name}</td>
                                        </tr>
                                        <tr>
                                            <td>Plot:</td>
                                            <td>${apartmentBlock.plot.plotNumber}</td>
                                        </tr>
                                        <tr>
                                            <td>Landlord</td>
                                            <td><a href="/admin/landlord/show/${apartmentBlock.plot.landlord.id}">
                                            ${apartmentBlock.plot.landlord.fullName}
                                            </a>
                                            </td>
                                        </tr>

                                        <tr>
                                        <tr>
                                            <td>Number of apartments</td>
                                            <td>${apartmentBlock.apartments.size()}</td>
                                        </tr>
                                        <tr>
                                            <td>Town:</td>
                                            <td>${apartmentBlock.plot.town.name}</td>
                                        </tr>
                                        <td>Phone Number</td>
                                        <td><br><br>${landlord.user.id}(Mobile)
                                        </td>

                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="box-footer clearfix">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <a href="/admin/plot/show/${apartmentBlock.plot.id}" class="btn btn-small">Back</a>
        </div>
        <div class="box-body">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-0 toppad" >


                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Apartments on this block</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="table-responsive">
                                        <c:choose>
                                            <c:when test="${empty apartmentBlock.apartments}">
                                                <div class="alert alert-info">
                                                    <strong>Empty!</strong> No Apartments to display.
                                                    <p><a href="/admin/apartment/form?id=${apartmentBlock.id}">Create New</a> </p>
                                                </div>
                                            </c:when>
                                            <c:otherwise>
                                                <table class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Number</th>
                                                        <th>Cost</th>
                                                        <th>State</th>
                                                        <th>Bedroom(s)</th>
                                                        <th></th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <c:forEach items="${apartmentBlock.apartments}" var="apartment">
                                                        <tr>
                                                            <td>${apartment.name}</td>
                                                            <td>${apartment.number}</td>
                                                            <td>${apartment.cost}</td>
                                                            <td>${apartment.state}</td>
                                                            <td>${apartment.bedroom}</td>
                                                            <td>
                                                                <a href="/admin/apartment/show/${apartment.id}">Show</a>
                                                            </td>
                                                            <td>
                                                                <a href="#" data-original-title="Edit this apartment" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning">
                                                                    <i class="glyphicon glyphicon-edit"></i>
                                                                </a>
                                                            </td>
                                                            <td>
                                                                <a href="#" data-original-title="Remove this apartment" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger">
                                                                    <i class="glyphicon glyphicon-remove"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </c:forEach>
                                                    </tbody>
                                                    <tfoot>
                                                    <p><a href="/admin/apartment/form?id=${apartmentBlock.id}">Add New</a> </p>
                                                    </tfoot>
                                                </table>
                                            </c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <a href="/admin/plot/show/${apartmentBlock.plot.id}" class="btn btn-small">Back</a>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
