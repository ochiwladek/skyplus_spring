<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <jsp:include page="/WEB-INF/jsp/common/adminHeader.jsp">
        <jsp:param name="title" value="Skyplus"/>
    </jsp:include>
</head>
<jsp:include page="/WEB-INF/jsp/common/adminHead.jsp"/>

<!-- Carousel
================================================== -->
<%--body--%>
<div class="box">
    <div class="box-header with-border">
        <h2 class="box-title">Apartment Blocks List</h2>
        <div class="box-tools">
            <div class="input-group">
                <input type="text" name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search by town"/>
                <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </div><!-- /.box-header -->
    <div class="box-body table-responsive no-padding">
        <c:choose>
            <c:when test="${empty apartmentBlockList}">
                <h5>Empty</h5>
            </c:when>
            <c:otherwise>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Block Name</th>
                            <th>Plot</th>
                            <th>Number of apartments</th>
                            <th>Description</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${apartmentBlockList}" var="block">
                          <tr>
                              <td>
                                  <img src="${block.asset.name}" height="55px" width="45px"
                                       alt="Profile photo">
                              </td>
                              <td>${block.name}</td>
                              <td>${block.plot.plotNumber}</td>
                              <td>${block.apartments.size()}</td>
                              <td>${block.description}</td>
                              <td>
                                  <a href="/admin/apartmentBlock/show/${block.id}">Show</a>
                              </td>
                              <td>
                                  <a href="# /admin/plot/edit/${plot.id}" data-original-title="Edit this plot" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning">
                                      <i class="glyphicon glyphicon-edit"></i>
                                  </a>
                              </td>
                              <td>
                                  <a href="/admin/plot/delete/${plot.id}" data-original-title="Remove this plot" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger">
                                      <i class="glyphicon glyphicon-remove"></i>
                                  </a>
                              </td>
                          </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
    <div class="box-footer clearfix">
        <span class="glyphicon glyphicon-plus"></span>
        <a href="#" class="btn btn-small"></a>
    </div>
</div>
<%--end body--%>
<!-- start of footer section -->

<jsp:include page="/WEB-INF/jsp/common/adminFooter.jsp"/>
</html>
