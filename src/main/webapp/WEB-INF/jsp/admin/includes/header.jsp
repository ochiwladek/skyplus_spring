<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <jsp:include page="/WEB-INF/jsp/common/head.jsp">
    <jsp:param name="title" value="Skyplus"/>
  </jsp:include>
  <!-- FONTAWESOME STYLES-->
  <link href="/resources/css/font-awesome.css" rel="stylesheet" />
  <%--custom css--%>
  <link href="/resources/css/custom.css" rel="stylesheet" />
  <%--google fonts--%>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>

<body>
<jsp:include page="/WEB-INF/jsp/common/adminPagesTopmenu.jsp"/>

<!-- Carousel
