<%--end body--%>
<!-- start of footer section -->
<!-- JQUERY SCRIPTS -->
<script src="/resources/js/jquery-1.10.2.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/resources/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/resources/js/custom.js"></script>
<!-- MORRIS CHART SCRIPTS -->
<script src="/resources/js/morris/raphael-2.1.0.min.js"></script>

<jsp:include page="/WEB-INF/jsp/common/footer.jsp"/>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
</html>
