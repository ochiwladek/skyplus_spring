<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="/resources/images/skyplus_only.png" height="50" width="auto"</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#"><span class="sr-only">(current)</span></a></li>
                <li><a href="#"></a></li>

            </ul>

            <ul class="nav navbar-nav navbar-right" id="nav-bar-links">
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
                <%--<li class="dropdown">--%>
                    <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Task <span class="caret"></span></a>--%>
                    <%--<ul class="dropdown-menu" role="menu">--%>
                        <%--<li><a href="/admin">Admin Home </a></li>--%>
                        <%--<li><a href="/admin/create-new-property">Create A new property</a></li>--%>
                        <%--<li><a href="#">View Property Lists</a></li>--%>
                        <%--<li><a href="#">Town</a></li>--%>
                        <%--<li><a href="#">Land</a></li>--%>
                        <%--<li><a href="#">Category</a></li>--%>
                        <%--<li class="divider"></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>
                </li>
                <li><a href="/users/logout" class="fa-fa power">Admin Logout</a></li>
                <!-- <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>

                  </ul>
                </li> -->
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<%--side nav--%>
<nav class="navbar-default navbar-side navbar-fixed-side">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="/resources/images/find_user.png" class="user-image img-responsive"/>
            </li>


            <li>
                <a class="active-menu"  href="/admin"><i class="fa fa-dashboard fa-3x"></i> Dashboard</a>
            </li>
            <li>
                <a href="#"><i class="glyphicon glyphicon-home"></i>Town/City<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/property/town/list">View all</a>
                    </li>
                    <li>
                        <a href="/admin/property/town">Add Town/city</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="glyphicon glyphicon-home"></i> Property Category<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/property/categoryList">Category List</a>
                    </li>
                    <li>
                        <a href="/admin/property/addCategory">Add Category</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="glyphicon glyphicon-home"></i> House<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/property/houseList">View All</a>
                    </li>
                    <li>
                        <a href="/admin/property/createProperty/house">Create New</a>
                    </li>
                    <li>
                        <a href="/admin/property/houseType/list">House types</a>
                    </li>
                    <%--<li>--%>
                        <%--<a href="#">Second Level Link<span class="fa arrow"></span></a>--%>
                        <%--<ul class="nav nav-third-level">--%>
                            <%--<li>--%>
                                <%--<a href="#">Third Level Link</a>--%>
                            <%--</li>--%>
                            <%--<li>--%>
                                <%--<a href="#">Third Level Link</a>--%>
                            <%--</li>--%>
                            <%--<li>--%>
                                <%--<a href="#">Third Level Link</a>--%>
                            <%--</li>--%>

                        <%--</ul>--%>

                    <%--</li>--%>
                </ul>
            </li>
            <li>
                <a href="#"><i class="glyphicon glyphicon-home"></i> Land<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="/admin/property/landList">View All</a>
                    </li>
                    <li>
                        <a href="/admin/property/createProperty/land">Create New</a>
                    </li>
                </ul>
            </li>
            <li  >
                <a  href="blank.html"><i class="fa fa-square-o fa-3x"></i> Blank Page</a>
            </li>
        </ul>

    </div>

</nav>
<!-- end of navigation bar -->



