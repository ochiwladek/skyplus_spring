<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<footer class="text-center">

    <div class="footer-below">
        <div class="container">
            <div class="row">

                <div class="col-lg-4">
                    <ul>
                        <p><a href="">About US</a></p>
                        <p><a href="">Contact Us</a></p>
                        <p><a href="">Services</a></p>

                    </ul>
                </div>
                <div class="col-lg-4">
                    <h3></h3>
                    <ul class="list-inline intro-social-buttons">
                        <li>
                            <a href="https://twitter.com" ><i class="fa fa-twitter fa-3x"></i> 
                            
                            <span class="network-name"></span></a>
                        </li>


                        <li>
                            <a href="https://facebook.com"><i class="fa fa-facebook fa-3x"></i> <span class="network-name"></span></a>
                        </li>
                        <li>
                            <a href="https://facebook.com"><i class="fa  fa-google-plus fa-3x"></i> <span class="network-name"></span></a>
                        </li>

                    </ul>







                    <p>Skyplus Real Estate 2015<p>
                </div>
                <div class="col-lg-4">
                    <p>Caxton House, Koinange Street, Opp. GPO, </p>
                    <p>  Second Floor, Room 5,</p>
                    <p> P.O. Box 103793-00101, Nairobi</p>
                    <p><i class="fa fa-mobile fa-fw"></i> 0725-380 152 | 0720-926 158</i></p>
                    <p> Email: info@skyplusagencies.com</p>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript" src="/resources/components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="application/javascript" src="/resources/components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    <script type="application/javascript" src="/resources/components/holderjs/holder.js"></script>
</footer>