<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<footer class="page-footer">
          <div class="container">
            <div class="row" id="myfooter">
              <div class="col l4 m4 s12">
                <h5 class="white-text"></h5>
                <p class="grey-text text-lighten-4">About Us</p>
                 <p class="grey-text text-lighten-4">Contact Us</p>
                  <p class="grey-text text-lighten-4">Houses</p>
                   <p class="grey-text text-lighten-4">Land</p>
              </div>
              <div class="col l4 m4 s12">
            
              <a href="https://twitter.com/SkyplusSky"><i class="fa fa-twitter fa-3x"></i></a>
               <a href="https://www.facebook.com/SkyplusAgencies"><i class="fa fa-facebook fa-3x"></i></a>
              <i class="fa fa-google-plus fa-3x"></i>
              
              </div>
              <div class="col l4 m4  s12">
                <h5 class="white-text">Location</h5>
                <ul>
                  <li>Kimathi House</li>
                  <li>Fourth Floor</li>
                  <li>P.O. Box 103793-00101, Nairobi</li>
                  <li><i class="fa fa-mobile fa-fw"></i> 0725-380 152 | 0720-926 158</li>
                   <li>Email: info@skyplusagencies.com</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2015 Skyplus Agencies
           
            </div>
          </div>
        </footer>
            



  <!-- materialize js -->
   <script type="text/javascript" src="/resources/components/materialize/dist/js/materialize.min.js"></script>
    <script type="application/javascript" src="/resources/components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
    <script type="application/javascript" src="/resources/components/holderjs/holder.js"></script>
    <script type="application/javascript" src="/resources/js/slider.js"></script>
    <script type="application/javascript" src="/resources/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript">
    $( document ).ready(function(){
    $(".button-collapse").sideNav();
    $('.modal-trigger').leanModal();
    })
    
</script>

<script type="text/javascript">
    $(document).ready(function(){

    $(".dropdown-button").dropdown();
        
       $('select').material_select();
        $('.tooltipped').tooltip({delay: 50});

        $('#skyplus').effect( "slide", "slow" );

        $( "#myslider" ).delay( 10000 ).hide( "clip", { direction: "right" }, 3000 ).queue(function(){

          $('#search').show("clip", 2000);
          $('#headbar').addClass( "header2", 2000, "linear" );
          

        });

        
    });
</script>
<script type="text/javascript">
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
  

</script>

<!-- google analytics -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65855563-1', 'auto');
  ga('send', 'pageview');

</script>
  

</script>


