<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- Dropdown Structure -->
<ul id="dropdown3" class="dropdown-content">
  <li><a href="/front/apartment/index?cat=Sale">Apartment</a></li>
  <li><a href="/front/home/index?category=Sale&type=bungalow">Bungalows</a></li>
  
  <li><a href="/front/home/index?category=Sale&type=mansion">Maisonettes</a></li>
   <li><a href="/front/land/index">Land</a></li>
</ul>

<ul id="dropdown4" class="dropdown-content">
              <li><a href="/front/apartment/index?cat=Rent">Apartments</a></li>
              <li><a href="/front/home/index?category=Rent&type=bungalow">Bungalows</a></li>
              <li><a href="/front/home/index?category=Rent&type=mansion">Maisonettes</a></li>
              <li><a href="/front/office/index">Offices</a></li>
</ul>



<ul id="dropdown1" class="dropdown-content">
  <li><a href="/front/property/sale/filter/apartment">Apartment</a></li>
  <li><a href="/front/property/sale/filter/bungalow">Bungalows</a></li>
  
  <li><a href="/front/property/sale/filter/maisonette">Maisonettes</a></li>
   <li><a href="/front/property/sale/filter/land">Land</a></li>
</ul>

<ul id="dropdown2" class="dropdown-content">
              <li><a href="/front/property/let/filter/apartment">Apartments</a></li>
              <li><a href="/front/property/let/filter/bungalow">Bungalows</a></li>
              <li><a href="/front/property/let/filter/maisonette">Maisonettes</a></li>
              <li><a href="/front/property/sale/filter/office">Offices</a></li>
</ul>



<div class="navbar-fixed">

<nav id="mynav">
    <div class="nav-wrapper">
        <a href="/" class="brand-logo left">Sky Plus Agencies</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
        <ul class="right hide-on-med-and-down">
          <li><a href="/">Home</a></li>
       <!-- Dropdown Trigger -->
      <li><a class="dropdown-button" href="#!" data-activates="dropdown1">For Sale</a></li>
      <li><a class="dropdown-button" href="#!" data-activates="dropdown2">To Let</a></li>
      
           
            <li><a href="/front/aboutUs">About Us</a></li>
            <li><a href="/front/contactUs">Contact Us</a></li>
            <li><a href="/front/faqs/index">FAQS</a></li>
            <sec:authorize access="isAnonymous()">
                <li><a href="/users/loginform">Log in</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <sec:authentication property="principal" var="principal"/>
                <li class="dropdown">
                    <a href="#">${principal.user.loginId}</a>
                    <ul class="sub-menu">
                        <li id="logoutBtn"><a href="/users/logout">Log out</a> </li>
                    </ul>
                </li>
            </sec:authorize>
        </ul>
       
        <ul class="side-nav" id="mobile-demo">
         <li><a href="/">Home</a></li>

         <li><a class="dropdown-button" href="#!" data-activates="dropdown3">For Sale</a></li>
      <li><a class="dropdown-button" href="#!" data-activates="dropdown4">To Let</a></li>
        
    
            <li><a href="/front/aboutUs">About Us</a></li>
            <li><a href="/front/contactUs">Contact Us</a></li>
            <li><a href="/front/faqs/index">FAQS</a></li>
           
            <sec:authorize access="isAnonymous()">
                <li><a href="/users/loginform">Log in</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <sec:authentication property="principal" var="principal"/>
                <li><a href="#">${principal.user.loginId}</a></li>
            </sec:authorize>
       
       
        
      </ul>
    

    </div>
  </nav>
  </div>

  <!-- facebook like,share and comment -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=824080394338805";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>





