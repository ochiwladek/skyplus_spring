<%--
  Created by IntelliJ IDEA.
  User: naver
  Date: 2014. 8. 8.
  Time: 오전 9:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="keywords" content="house rent for sale for rent apartment bungalows flats cheap affordable kenya number 1 kitchen bathroom living room bedrooms safe secure clean security">
<meta name="description" content="Find property for sale and Rent. Search over 900,000 properties for sale from the top estate agents and developers in Kenya">
<meta name="author" content="">

<title>${param.title}</title>

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />

<!-- google fonts -->
<link href='http://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>



<!-- Materialize CSS -->

<link rel="stylesheet" type="text/css" href="/resources/components/materialize/dist/css/materialize.min.css">

<%--custom css--%>

<link rel="stylesheet" type="text/css" href="/resources/css/new_landing.css">
<link rel="stylesheet" type="text/css" href="/resources/css/landingPageLinks.css">
<link rel="stylesheet" type="text/css" href="/resources/css/style.css">





<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="application/javascript" src="/resources/components/jquery/dist/jquery.min.js"></script>


<script type="application/javascript" src="/resources/js/ie10-viewport-bug-workaround.js"></script>

<script type="application/javascript" src="/resources/js/ie10-viewport-bug-workaround.js"></script>




    <!-- End of head section HTML codes -->



<!-- font awesome -->
<link rel="stylesheet" type="text/css" href="/resources/components/font-awesome/css/font-awesome.min.css">
<%--<script type="text/javascript" src="/resources/js/tinymce.min.js"></script>--%>
<%--<script>tinymce.init({selector:'textarea'});</script>--%>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script type="application/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script type="application/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Custom styles for this template -->

<!-- Start WOWSlider.com HEAD section -->
<link rel="stylesheet" type="text/css" href="/resources/images/wow/engine1/style.css" />

<!-- End WOWSlider.com HEAD section -->
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10534759; 
var sc_invisible=1; 
var sc_security="7a2eab3e"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify site
analytics" href="http://statcounter.com/shopify/"
target="_blank"><img class="statcounter"
src="http://c.statcounter.com/10534759/0/7a2eab3e/1/"
alt="shopify site analytics"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->

