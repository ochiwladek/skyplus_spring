<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="navbar-fixed">
<ul id="dropdown1" class="dropdown-content">
<li id="categories"><a href="#"></a></li>
         
</ul>
<nav id="mynav3">
    <div class="nav-wrapper">
        <a href="/" class="brand-logo left">Sky Plus Agencies</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
        <ul class="right hide-on-med-and-down">
            <li class="dropdown">
                <a href="#">Property</a>
                <ul>
                    <li><a href="#">For Sale</a>
                        <ul>
                            <li><a href="/front/property/sale/filter/apartment">Apartments</a></li>
                            <li><a href="/front/property/sale/filter/bungalow">Bungalows</a></li>
                            <li><a href="/front/property/sale/filter/maisonette">Maisonettes</a></li>
                            <li><a href="/front/property/sale/filter/land">Land</a></li>
                        </ul>
                    </li>
                    <li><a href="">For Rent</a>
                        <ul>
                            <li><a href="/front/property/let/filter/apartment">Apartments</a></li>
                            <li><a href="/front/property/let/filter/bungalow">Bungalows</a></li>
                            <li><a href="/front/property/let/filter/maisonette">Maisonettes</a></li>
                            <li><a href="/front/property/let/filter/office">Offices</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="/front/aboutUs">About Us</a></li>
            <li><a href="/front/contactUs">Contact Us</a></li>
            <li><a href="/front/faqs/index">FAQS</a></li>
            <sec:authorize access="isAnonymous()">
                <li><a href="/users/loginform">Log in</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <sec:authentication property="principal" var="principal"/>
                <li class="dropdown">
                    <a href="#">${principal.user.loginId}</a>
                    <ul class="sub-menu">
                        <li id="logoutBtn"><a href="/users/logout">Log out</a> </li>
                    </ul>
                </li>
            </sec:authorize>
        </ul>
    </div>
  </nav>
  </div>
<script>
    $(document).ready(function() {
        $('.sub-menu').slideUp(200);
        $( '.dropdown' ).hover(
                function(){
                    $(this).children('.sub-menu').slideDown(200);
                },
                function(){
                    $(this).children('.sub-menu').slideUp(200);
                }
        );
         
    });
</script>





