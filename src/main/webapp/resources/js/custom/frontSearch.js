(function($) {
  $(function(){
    $('#searchBtn').click(function(e){
      e.preventDefault();
      submitFilter();
    })
  });

  function submitFilter(){
    var townId = $('#townId').select().val();
    var categoryId = $('#categoryId').select().val();
    var subCategoryId = $('#subCategoryId').select().val();
    var propertyType = $('#propertyType').select().val();

    $.ajax({
      url:'/front/search',
      dataType: 'json',
      type: 'POST',
      data:{
        townId: townId , categoryId : categoryId , subCategoryId : subCategoryId , propertyType : propertyType
      }
    }).done(function(result,status,xhr){
      console.log(status);
      console.log(xhr);
      console.log(result);
    }).fail(function(result,status,xhr){
      console.log(status);
      console.log(xhr);
      console.log(result);
    })
  }

})(jQuery)
