package com.skyplus.agency.web.front;

import com.skyplus.agency.domain.general.PasswordRecoveryPojo;
import com.skyplus.agency.domain.tokens.PasswordRecovery;
import com.skyplus.agency.service.token.TokenService;
import com.skyplus.agency.web.front.support.PasswordEmailValidator;
import com.skyplus.agency.web.front.support.PasswordRecoveryValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * Created by wladek on 7/29/15.
 */
@Controller
@RequestMapping(value = "/front/account")
public class AccountsController {
    @Autowired TokenService tokenService;
    @Autowired PasswordEmailValidator passwordEmailValidator;
    @Autowired PasswordRecoveryValidator passwordRecoveryValidator;
    @RequestMapping(value = "/activate-account/{id}/{token}" , method = RequestMethod.GET)
    public String activate(@PathVariable("id")Long id , @PathVariable("token") String token , Model model){
        String message = tokenService.activateAccount(id, token);

        model.addAttribute("message" , message);

        return "/front/users/activationSuccess";
    }

    @RequestMapping(value = "/password-recovery" , method = RequestMethod.GET)
    public String userEmailForm(Model model){
        model.addAttribute("passwordRecovery" , new PasswordRecovery());
        return "/front/users/userEmailForm";
    }

    @RequestMapping(value = "/password-recovery" , method = RequestMethod.POST)
    public String postUserEmailForm(@Valid @ModelAttribute("passwordRecovery") PasswordRecovery passwordRecovery ,
                                    BindingResult bindingResult , Model model){
        passwordEmailValidator.validateEmail(passwordRecovery , bindingResult);
        if (bindingResult.hasErrors()){
            model.addAttribute("passwordRecovery" , passwordRecovery);
            return "/front/users/userEmailForm";
        }
        tokenService.sendPasswordResetToken(passwordRecovery.getEmail());
        model.addAttribute("message" , "Success . A password Reset link has been sent to your email");
        return "/front/users/linkSuccess";
    }

    @RequestMapping(value = "/password-recovery/{id}/{token}" , method = RequestMethod.GET)
    public String getPasswordResetForm(@PathVariable("id") Long id , @PathVariable("token") String token , Model model){
        model.addAttribute("passwordRecoveryPojo" , new PasswordRecoveryPojo());
        model.addAttribute("token" , token);
        return "/front/users/passwordForm";
    }

    @RequestMapping(value = "/password-recovery/reset" , method = RequestMethod.POST)
    public String resetPassword(@Valid @ModelAttribute("passwordRecoveryPojo") PasswordRecoveryPojo passwordRecoveryPojo ,
                                BindingResult bindingResult , Model model){
        passwordRecoveryValidator.validatePassword(passwordRecoveryPojo , bindingResult);
        if (bindingResult.hasErrors()){
            model.addAttribute("passwordRecoveryPojo" , passwordRecoveryPojo);
            return "/front/users/passwordForm";
        }

       String message =  tokenService.resetPassword(passwordRecoveryPojo);
        model.addAttribute("message" , message+" .Please Log in");
        return "/front/users/linkSuccess";
    }
}
