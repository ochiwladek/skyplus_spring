package com.skyplus.agency.web.front;

import com.skyplus.agency.domain.Faqs;
import com.skyplus.agency.service.faqs.FaqsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by wladek on 8/28/15.
 */
@Controller
@RequestMapping(value = "/front/faqs")
public class FrontFaqsController {
    @Autowired FaqsService faqsService;

    @RequestMapping(value = "/index" , method = RequestMethod.GET)
    public String index(Model model){
        List<Faqs> faqsList = faqsService.findAll();

        model.addAttribute("faqsList" , faqsList);
        return "/front/faqs/index";
    }

    @RequestMapping(value = "/read-more" , method = RequestMethod.GET)
    public String readMore(@RequestParam("faqId") Long id, Model model){
        List<Faqs> faqsList = faqsService.findAll();

        Faqs faq = faqsService.findOne(id);

        model.addAttribute("faqsList" , faqsList);
        model.addAttribute("selectedFaq" , faq);

        return "/front/faqs/index";
    }
}
