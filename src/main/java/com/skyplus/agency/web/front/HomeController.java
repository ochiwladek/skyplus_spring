package com.skyplus.agency.web.front;

import com.skyplus.agency.domain.Property;
import com.skyplus.agency.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by wladek on 4/26/15.
 */
@Controller
public class HomeController {
    @Autowired
    PropertyService propertyService;
    @RequestMapping(value = "/" , method = RequestMethod.GET)
    public String getHomePage(@RequestParam(value = "page",required = false, defaultValue = "1") int page,
                              @RequestParam(value = "size", required = false, defaultValue = "10") int size ,
                              Model model){

        Page<Property> propertyPage = propertyService.findAll(page, size);

         model.addAttribute("propertyPage" , propertyPage);
         model.addAttribute("pagenatedUrl" , "/");
        return "/index";
    }
}
