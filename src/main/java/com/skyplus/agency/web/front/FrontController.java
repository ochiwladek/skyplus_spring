package com.skyplus.agency.web.front;

import com.skyplus.agency.domain.*;
import com.skyplus.agency.service.ContactUsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by wladek on 4/27/15.
 */
@Controller
@RequestMapping(value = "/front")
public class FrontController {

    @Autowired ContactUsService contactUsService;

    @RequestMapping(value = "/aboutUs" , method = RequestMethod.GET)
    public String getAboutUsPage(){
        return "/front/users/aboutUs";
    }

    @RequestMapping(value = "/contactUs" , method = RequestMethod.GET)
    public String getContactUsForm(Model model){
        model.addAttribute("contactUsDetails" , new ContactUsDetails());
        return "/front/users/contactUsForm";
    }

    @RequestMapping(value = "/contactUs" , method = RequestMethod.POST)
    public String postContactUsDetails(@Valid @ModelAttribute("contactUsDetails") ContactUsDetails contactUsDetails,
                                       BindingResult bindingResult , RedirectAttributes redirectAttributes){

        if (bindingResult.hasErrors()){
            return "/front/users/contactUsForm";
        }
        String message = contactUsService.contactUs(contactUsDetails);
        redirectAttributes.addFlashAttribute("success" , true);
        redirectAttributes.addFlashAttribute("message","Thank you for your feedback, we will get back to you");
        return "redirect:/front/contactUs";
    }

    @RequestMapping(value = "/reg-success" , method = RequestMethod.GET)
    public String regSuccess(){
        return "/front/users/signupSuccess";
    }
}
