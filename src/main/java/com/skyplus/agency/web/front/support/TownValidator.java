package com.skyplus.agency.web.front.support;

import com.skyplus.agency.domain.Town;
import com.skyplus.agency.repository.admin.category.TownRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

/**
 * @author Keeun Baik
 */
@Component
public class TownValidator {

    @Autowired
    TownRepository townRepository;

    public boolean validateNewTown(Town town, BindingResult result) throws NullPointerException{

        Town existingTown = townRepository.findByName(town.getName());
            if(existingTown != null) {
                result.rejectValue("name", "town.name.duplicate", "That town already exists, choose another name");
            }

        return result.hasErrors();
    }
}
