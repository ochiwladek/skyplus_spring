package com.skyplus.agency.web.front.support;

import com.skyplus.agency.domain.general.PasswordRecoveryPojo;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * @author Keeun Baik
 */
@Component
public class PasswordRecoveryValidator {

    public boolean validatePassword(PasswordRecoveryPojo passwordRecovery, BindingResult result) throws NullPointerException {

        if (!passwordRecovery.getPassword().equals(passwordRecovery.getPassword2())) {
            result.rejectValue("password", "passwordRecovery.password.duplicate", "Your passwords do not match , Please try again");
            result.rejectValue("password2", "passwordRecovery.password2.duplicate", "Your passwords do not match , Please try again");
        }

        return result.hasErrors();
    }
}
