package com.skyplus.agency.web.front;

import com.skyplus.agency.domain.ContactUsDetails;
import com.skyplus.agency.domain.Property;
import com.skyplus.agency.service.ContactUsService;
import com.skyplus.agency.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by wladek on 4/27/15.
 */
@Controller
@RequestMapping(value = "/front/property")
public class FrontPropertyController {

    @Autowired
    PropertyService propertyService;

    @RequestMapping(value = "/show/{id}" , method = RequestMethod.GET)
    public String showProperty(@PathVariable("id") Long id , Model model){
        Property property = propertyService.getOne(id);
        model.addAttribute("property" , property);
        return "/front/property/show";
    }

    @RequestMapping(value = "/all")
    public String showAll(@RequestParam(value = "page",required = false, defaultValue = "1") int page,
                              @RequestParam(value = "size", required = false, defaultValue = "10") int size ,
                              Model model){

        Page<Property> propertyPage = propertyService.findAll(page, size);

        model.addAttribute("propertyPage" , propertyPage);
        model.addAttribute("pagenatedUrl" , "/front/property/all");
        return "/front/property/index";
    }

    @RequestMapping(value = "/{category}/filter/{type}")
    public String showFiltered(@PathVariable("category") String category , @PathVariable("type") String type,
                               @RequestParam(value = "page",required = false, defaultValue = "1") int page,
                               @RequestParam(value = "size", required = false, defaultValue = "10") int size , Model model){

        Page<Property> propertyPage = propertyService.getByPaged(category , type , page ,size);

        model.addAttribute("propertyPage" , propertyPage);
        model.addAttribute("pagenatedUrl" , "/front/property/all");

        return "/front/property/index";
    }
}
