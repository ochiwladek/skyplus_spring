package com.skyplus.agency.web.front;

import com.skyplus.agency.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by wladek on 8/31/15.
 */
@Controller
@RequestMapping(value = "/front/sms/test")
public class NewTestController {
    @Autowired SmsService smsService;

    @RequestMapping(value = "/" , method = RequestMethod.GET)
    public String testSms(){
        String to = "0705712841";
        String sms = "Test sms";

        try {
            smsService.sendMessage(to , sms);
        }catch (Exception e){
            e.printStackTrace();
        }

        return "redirect:/front/contactUs";
    }
}
