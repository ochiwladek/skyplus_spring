package com.skyplus.agency.web.front.support;

import com.skyplus.agency.domain.User;
import com.skyplus.agency.domain.tokens.PasswordRecovery;
import com.skyplus.agency.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * @author Keeun Baik
 */
@Component
public class PasswordEmailValidator {

    @Autowired
    UserRepository repository;

    public boolean validateEmail(PasswordRecovery passwordRecovery, BindingResult result) throws NullPointerException{
        List<User> userList = repository.findAll();

        try {
            if (!passwordRecovery.getEmail().equals(passwordRecovery.getEmail2())){
                result.rejectValue("email" , "passwordRecovery.password.duplicate" , "Your emails do not match , Please try again");
                result.rejectValue("email2", "passwordRecovery.password2.duplicate", "Your emails do not match , Please try again");
            }

            User existingUser = repository.findByEmail(passwordRecovery.getEmail());
            if(existingUser == null) {
                result.rejectValue("email", "passwordRecovery.email1.duplicate", " Unregistered email addresses !");
                result.rejectValue("email2", "passwordRecovery.email2.duplicate", " Unregistered email addresses !");
            }
        }catch (NullPointerException ex){
            ex.printStackTrace();

            User existingUser = repository.findByEmail(passwordRecovery.getEmail());
            if(existingUser == null) {
                result.rejectValue("email", "passwordRecovery.email1.duplicate", " Unregistered email addresses !");
                result.rejectValue("email2", "passwordRecovery.email2.duplicate", " Unregistered email addresses !");
            }

        }

        return result.hasErrors();
    }
}
