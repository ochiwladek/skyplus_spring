package com.skyplus.agency.web.front.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import com.skyplus.agency.domain.User;
import com.skyplus.agency.repository.UserRepository;

import java.util.List;

/**
 * @author Keeun Baik
 */
@Component
public class UserValidator {

    @Autowired
    UserRepository repository;

    public boolean validateNewUser(User user, BindingResult result) throws NullPointerException{
        List<User> userList = repository.findAll();

        try {
            if (!user.getPassword().equals(user.getPassword2())){
                result.rejectValue("password" , "user.password.duplicate" , "Your passwords do not match , Please try again");
                result.rejectValue("password2", "user.password2.duplicate", "Your passwords do not match , Please try again");
            }

            User existingUser = repository.findByEmail(user.getEmail());
            if(existingUser != null) {
                result.rejectValue("email", "user.email.duplicate", "Their exists a user with that email !");
            }

            existingUser = repository.findByLoginId(user.getLoginId());
            if(existingUser != null) {
                result.rejectValue("loginId", "user.loginId.duplicate", "Their exists a user with that username !");
            }
        }catch (NullPointerException ex){
            ex.printStackTrace();

            User existingUser = repository.findByEmail(user.getEmail());
            if(existingUser != null) {
                result.rejectValue("email", "user.email.duplicate", "Their exists a user with that email !");
            }

            existingUser = repository.findByLoginId(user.getLoginId());
            if(existingUser != null) {
                result.rejectValue("loginId", "user.loginId.duplicate", "Their exists a user with that username !");
            }

            if (!user.getPassword().equals(user.getPassword2())){
                result.rejectValue("password" , "user.password.duplicate" , "Your passwords do not match , Please try again");
                result.rejectValue("password2", "user.password2.duplicate", "Your passwords do not match , Please try again");
            }

        }

        return result.hasErrors();
    }
}
