package com.skyplus.agency.web.admin;

import com.skyplus.agency.domain.Property;
import com.skyplus.agency.domain.enumeration.Category;
import com.skyplus.agency.repository.admin.category.AssetRepository;
import com.skyplus.agency.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by wladek on 5/25/15.
 */
@Controller
@RequestMapping(value = "/admin/property")
public class PropertyController {
    @Autowired
    PropertyService propertyService;

    @RequestMapping(value = "/" , method = RequestMethod.GET)
    public String index(@RequestParam(value = "size" , required = false, defaultValue = "10") int size ,
                        @RequestParam(value = "page" , required = false, defaultValue = "1") int page , Model model){

        Page<Property> propertyPage= propertyService.findAll(page, size);
        model.addAttribute("propertyPage" , propertyPage);
        model.addAttribute("pagenatedUrl" , "/admin/property/");
        return "/admin/property/index";
    }

    @RequestMapping(value = "/form" ,method = RequestMethod.GET)
    public String getTownForm(Model model){
        model.addAttribute("action" , "create");
        model.addAttribute("property" , new Property());
        return "/admin/property/form";
    }

    @RequestMapping(value = "/form/{action}" , method = RequestMethod.POST)
    public String postForm(@Valid @ModelAttribute("property") Property property , BindingResult bindingResult ,
                           @RequestParam("file") MultipartFile multipartFile, @PathVariable("action") String action ,
                           RedirectAttributes redirectAttributes, Model model) {

        if (action.equals("create")) {
            if (bindingResult.hasErrors()){

                model.addAttribute("action" , "create");
                model.addAttribute("property" , property);

                return "/admin/property/form";
            }

            property.setMultipartFile(multipartFile);
            property.setCategory(Category.valueOf(property.getCategoryId().toUpperCase()));
            Property newProperty = propertyService.create(property);

            redirectAttributes.addFlashAttribute("success" , true);
            redirectAttributes.addFlashAttribute("message" , newProperty.getName()+" successfully added");

            return "redirect:/admin/property/show/"+property.getId();
        }

        if (action.equals("edit")) {

            if (bindingResult.hasErrors()){
                property.setAsset(propertyService.getOne(property.getId()).getAsset());
                model.addAttribute("action" , "create");
                model.addAttribute("property" , property);

                return "/admin/property/form";
            }

            if (multipartFile != null && multipartFile.getSize() > 0){
                property.setMultipartFile(multipartFile);
            }

            Property newProperty = propertyService.update(property);

            redirectAttributes.addFlashAttribute("success" , true);
            redirectAttributes.addFlashAttribute("message" , newProperty.getName()+" successfully updated");

            return "redirect:/admin/property/show/"+property.getId();
        }
        return ":";
    }

    @RequestMapping(value = "/show/{id}" , method = RequestMethod.GET)
    public String showProperty(@PathVariable("id") Long id , Model model){
        Property property = propertyService.getOne(id);
        model.addAttribute("property" , property);
        return "/admin/property/show";
    }

    @RequestMapping(value = "/delete/{id}" , method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id , RedirectAttributes redirectAttributes){
        boolean result = propertyService.delete(id);
        redirectAttributes.addFlashAttribute("success" , result);
        redirectAttributes.addFlashAttribute("message" , " successfully deleted");
        return "redirect:/admin/property/";
    }

    @RequestMapping(value = "/edit/{id}" , method = RequestMethod.GET)
    public String editForm(@PathVariable("id") Long id , Model model){
        Property property = propertyService.getOne(id);
        model.addAttribute("action", "edit");
        model.addAttribute("property", property);
        return "/admin/property/form";
    }
}
