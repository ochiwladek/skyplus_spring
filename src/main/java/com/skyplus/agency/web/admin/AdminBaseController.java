package com.skyplus.agency.web.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by wladek on 1/18/16.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminBaseController {
    @RequestMapping(value = "/" , method = RequestMethod.GET)
    public String index(){
        return "/admin/index";
    }
}
