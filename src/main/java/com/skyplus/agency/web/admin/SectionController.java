package com.skyplus.agency.web.admin;

import com.skyplus.agency.domain.Section;
import com.skyplus.agency.repository.SectionRepo;
import com.skyplus.agency.service.PropertyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

/**
 * Created by wladek on 3/2/16.
 */
@Controller
@RequestMapping(value = "/admin/section")
public class SectionController {
    @Autowired
    PropertyService propertyService;
    @Autowired
    SectionRepo repo;

    @RequestMapping(value = "/{id}/form" , method = RequestMethod.GET)
    public String getForm(@PathVariable("id") Long propertyId , Model model){

        Section section = new Section();
        section.setPropertyId(propertyId);

        model.addAttribute("section" , section);
        model.addAttribute("action" , "create");

        return "/admin/section/form";
    }


    @RequestMapping(value = "/{action}/form" , method = RequestMethod.POST)
    public String postForm(@Valid @ModelAttribute("section") Section section , BindingResult bindingResult,
                           @PathVariable("action") String action, @RequestParam("file") MultipartFile multipartFile,
                           Model model){

        if(action.equals("create")){
            if(bindingResult.hasErrors()){

                model.addAttribute("section" , section);
                model.addAttribute("action" , "create");

                return "/admin/section/form";
            }

            section.setMultipartFile(multipartFile);
            section = propertyService.addSection(section);

            return "redirect:/admin/section/"+section.getId()+"/show";


        }
        model.addAttribute("section" , section);
        model.addAttribute("action" , "create");

        return ":";
    }

    @RequestMapping(value = "/{id}/show" , method = RequestMethod.GET)
    public String show(@PathVariable("id") Long id , Model model){

        Section section = repo.findOne(id);

        model.addAttribute("section" , section);

        return "/admin/section/show";
    }
}
