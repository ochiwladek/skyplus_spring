package com.skyplus.agency.web.admin;

import com.skyplus.agency.domain.Faqs;
import com.skyplus.agency.service.faqs.FaqsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by wladek on 8/28/15.
 */
@Controller
@RequestMapping(value = "/admin/faqs")
public class FaqsBackendController {
    @Autowired FaqsService faqsService;

    @RequestMapping(value = "/index" , method = RequestMethod.GET)
    public String index(Model model){
        List<Faqs> faqsList = faqsService.findAll();

        model.addAttribute("faqsList" , faqsList);
        return "/admin/faqs/index";
    }

    @RequestMapping(value = "/form" , method = RequestMethod.GET)
    public String form(Model model){
        model.addAttribute("faqs" , new Faqs());
        model.addAttribute("action" , "create");
        return "/admin/faqs/form";
    }

    @RequestMapping(value = "/edit/{id}" , method = RequestMethod.GET)
    public String editForm(@PathVariable("id") Long id , Model model){
        Faqs faqsInDb = faqsService.findOne(id);

        model.addAttribute("faqs" , faqsInDb);
        model.addAttribute("action" , "edit");
        model.addAttribute("faqId" , faqsInDb.getId());
        return "/admin/faqs/form";
    }

    @RequestMapping(value = "/form/{action}" , method = RequestMethod.POST)
    public String postForm(@Valid @ModelAttribute("faqs") Faqs faqs , BindingResult result ,
                           @PathVariable("action") String action , RedirectAttributes redirectAttributes , Model model){
        if (action.equals("create")){
            if (result.hasErrors()){
                model.addAttribute("faqs" , faqs);
                model.addAttribute("action" , "create");
                return "/admin/faqs/form";
            }

            Faqs newFaqs = faqsService.create(faqs);

            redirectAttributes.addFlashAttribute("success" , true);
            redirectAttributes.addFlashAttribute("message" , "FAQ successfully created");
            return "redirect:/admin/faqs/index";
        }

        if (action.equals("edit")){
            if (result.hasErrors()){
                model.addAttribute("faqs" , faqs);
                model.addAttribute("action" , "edit");
                model.addAttribute("faqId" , faqs.getFaqId());
                return "/admin/faqs/form";
            }

            Faqs faqsInDb = faqsService.findOne(faqs.getFaqId());

            faqsInDb.setQuestion(faqs.getQuestion());
            faqsInDb.setAnswer(faqs.getAnswer());

            Faqs editedFaqs = faqsService.create(faqsInDb);

            redirectAttributes.addFlashAttribute("success" , true);
            redirectAttributes.addFlashAttribute("message" , "FAQ successfully edited");
            return "redirect:/admin/faqs/index";
        }
        return null;

    }

    @RequestMapping(value = "/delete/{id}" , method = RequestMethod.GET)
    public String delete(@PathVariable("id") Long id , RedirectAttributes redirectAttributes){
        Faqs faqInDb = faqsService.findOne(id);
        faqsService.delete(faqInDb);

        redirectAttributes.addFlashAttribute("success" , true);
        redirectAttributes.addFlashAttribute("message" , "FAQ successfully deleted");
        return "redirect:/admin/faqs/index";
    }

    @RequestMapping(value = "/show/{id}" , method = RequestMethod.GET)
    public String show(@PathVariable("id") Long id , Model model){
        Faqs faqInDb = faqsService.findOne(id);

        model.addAttribute("faq" , faqInDb);
        return "/admin/faqs/show";
    }
}
