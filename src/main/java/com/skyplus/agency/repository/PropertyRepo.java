package com.skyplus.agency.repository;

import com.skyplus.agency.domain.Property;
import com.skyplus.agency.domain.enumeration.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by wladek on 1/11/16.
 */
@Repository
public interface PropertyRepo extends JpaRepository<Property , Long> {
    public List<Property>  findByCategoryAndSubcategory(Category category , String subcategory);
    public List<Property> findByCategory(Category category);
    public Page<Property> findByCategoryAndType(Category category , String type, Pageable pageable);
}
