package com.skyplus.agency.repository.admin.category;

import com.skyplus.agency.domain.Town;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wladek on 5/18/15.
 */
@Repository
public interface TownRepository extends JpaRepository<Town , Long> {
    public Town findByName(String name);
}
