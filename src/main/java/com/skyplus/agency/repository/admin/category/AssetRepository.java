package com.skyplus.agency.repository.admin.category;

import com.skyplus.agency.domain.Asset;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wladek on 4/26/15.
 */
@Repository
public interface AssetRepository extends JpaRepository<Asset , Long> {
}
