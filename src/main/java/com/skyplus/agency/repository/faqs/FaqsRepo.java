package com.skyplus.agency.repository.faqs;

import com.skyplus.agency.domain.Faqs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wladek on 8/28/15.
 */
@Repository
public interface FaqsRepo extends JpaRepository<Faqs , Long>{
}
