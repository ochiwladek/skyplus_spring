package com.skyplus.agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.skyplus.agency.domain.User;

/**
 * @author Keeun Baik
 */
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

    User findByLoginId(String loginId);

    User findByEmail(String email);
}
