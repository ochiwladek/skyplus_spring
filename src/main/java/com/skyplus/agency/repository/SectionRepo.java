package com.skyplus.agency.repository;

import com.skyplus.agency.domain.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wladek on 3/2/16.
 */
@Repository
public interface SectionRepo extends JpaRepository<Section , Long> {
}
