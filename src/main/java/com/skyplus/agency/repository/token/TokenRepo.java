package com.skyplus.agency.repository.token;

import com.skyplus.agency.domain.tokens.Tokens;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wladek on 7/24/15.
 */
@Repository
public interface TokenRepo extends JpaRepository<Tokens , Long> {
    public Tokens findByTokenString(String token);
}
