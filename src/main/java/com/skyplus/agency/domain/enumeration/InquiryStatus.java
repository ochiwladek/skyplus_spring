package com.skyplus.agency.domain.enumeration;

/**
 * Created by wladek on 1/3/10.
 */
public enum InquiryStatus {
    NEW , READ , UNREAD
}
