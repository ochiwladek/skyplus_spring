package com.skyplus.agency.domain.enumeration;

/**
 * @author Keeun Baik
 */
public enum UserRole {
    SUPER_ADMIN , ADMIN, TENANT , LANDLORD , CARETAKER, USER;
}
