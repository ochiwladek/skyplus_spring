package com.skyplus.agency.domain.enumeration;

/**
 * Created by wladek on 7/24/15.
 */
public enum TokenType {
    ACCOUNT_ACTIVATION , PASSWORD_RECOVERY
}
