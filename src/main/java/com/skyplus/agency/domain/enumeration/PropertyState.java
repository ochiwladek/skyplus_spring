package com.skyplus.agency.domain.enumeration;

/**
 * Created by wladek on 6/27/15.
 */
public enum PropertyState {
    VACANT , AVAILABLE , OCCUPIED , SOLD , ON_REPAIR
}
