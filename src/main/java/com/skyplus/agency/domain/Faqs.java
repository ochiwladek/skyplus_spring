package com.skyplus.agency.domain;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Transient;

/**
 * Created by wladek on 8/28/15.
 */
@Entity
public class Faqs extends AbstractModel{
    @NotEmpty(message = "Cannot be left blank")
    @Length(min = 10 , max = 100 , message = "Must be between 10 to 100 charaters in length")
    private String question;
    @NotEmpty(message = "Cannot be left blank")
    @Length(min = 50 , max = 500, message = "Must be between 50 to 500 charaters long")
    private String answer;

    @Transient
    private Long faqId;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getFaqId() {
        return faqId;
    }

    public void setFaqId(Long faqId) {
        this.faqId = faqId;
    }
}
