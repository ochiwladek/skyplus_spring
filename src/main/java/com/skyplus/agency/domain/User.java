/**
 * Yobi, Project Hosting SW
 *
 * Copyright 2012 NAVER Corp.
 * http://yobi.io
 *
 * @Author Ahn Hyeok Jun
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.skyplus.agency.domain;

import com.skyplus.agency.domain.enumeration.UserRole;
import com.skyplus.agency.domain.enumeration.UserState;
import com.skyplus.agency.domain.tokens.Tokens;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;

@Entity
@Table(name="users")
public class User extends AbstractModel{

    public static final String LOGIN_ID_PATTERN = "^[a-zA-Z0-9-]+([_.][a-zA-Z0-9-]+)*$";

    @Column(unique = true, nullable = false)
    @NotEmpty(message = "You must provide a username/login id")
    @Pattern(regexp = LOGIN_ID_PATTERN, message = "Please provide a valid user name comprising of alphanumerics only")
    private String loginId;

    @Column(nullable = false)
    @NotEmpty(message = "You must provide a password")
    private String password;

    @Transient
    @NotEmpty(message = "You must provide a password confirmation")
    private String password2;

    @Column(unique = true)
    @NotEmpty(message = "You must provide an email address")
    @Email(message = "Please provide a valid email address")
    private String email;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Enumerated(EnumType.STRING)
    private UserState userState;

    @OneToMany(mappedBy = "user" , cascade = CascadeType.ALL)
    private List<Tokens> tokensList;

    @OneToOne(mappedBy = "user" , cascade = {CascadeType.REMOVE , CascadeType.PERSIST})
    private AccountSettings accountSettings;


    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public UserState getUserState() {
        return userState;
    }

    public void setUserState(UserState userState) {
        this.userState = userState;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public List<Tokens> getTokensList() {
        return tokensList;
    }

    public void setTokensList(List<Tokens> tokensList) {
        this.tokensList = tokensList;
    }

    public AccountSettings getAccountSettings() {
        return accountSettings;
    }

    public void setAccountSettings(AccountSettings accountSettings) {
        this.accountSettings = accountSettings;
    }

}
