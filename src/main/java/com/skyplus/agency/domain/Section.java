package com.skyplus.agency.domain;

import com.skyplus.agency.domain.enumeration.Category;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

/**
 * Created by wladek on 1/11/16.
 */
@Entity
public class Section extends AbstractModel{
    private String name;
    private String description;
    @OneToOne(fetch = FetchType.LAZY)
    private Asset asset;
    @ManyToOne
    private Property property;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    private MultipartFile multipartFile;
    @Transient
    private Long propertyId;

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public Long getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Long propertyId) {
        this.propertyId = propertyId;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }
}
