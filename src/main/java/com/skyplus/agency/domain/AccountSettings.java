package com.skyplus.agency.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

/**
 * Created by wladek on 6/8/15.
 * This will contain the email subscriptions settings i.e when to send a notification
 * to an email subscriber
 */
@Entity
public class AccountSettings extends AbstractModel{
    private boolean houseCreated;
    private boolean houseBought;
    private boolean landCreated;
    private boolean landBought;

    @OneToOne(cascade = CascadeType.PERSIST)
    private User user;

    public boolean isHouseCreated() {
        return houseCreated;
    }

    public void setHouseCreated(boolean houseCreated) {
        this.houseCreated = houseCreated;
    }

    public boolean isHouseBought() {
        return houseBought;
    }

    public void setHouseBought(boolean houseBought) {
        this.houseBought = houseBought;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isLandCreated() {
        return landCreated;
    }

    public void setLandCreated(boolean landCreated) {
        this.landCreated = landCreated;
    }

    public boolean isLandBought() {
        return landBought;
    }

    public void setLandBought(boolean landBought) {
        this.landBought = landBought;
    }
}
