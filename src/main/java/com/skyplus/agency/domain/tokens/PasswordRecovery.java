package com.skyplus.agency.domain.tokens;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by wladek on 7/29/15.
 */
public class PasswordRecovery {
    @NotEmpty(message = "Please provide an email")
    @Email(message = "Please provide a valid email address")
    private String email;
    @NotEmpty(message = "Please provide a confirmation email")
    @Email(message = "Please provide a valid email address")
    private String email2;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }
}
