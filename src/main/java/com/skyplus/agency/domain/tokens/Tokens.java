package com.skyplus.agency.domain.tokens;

import com.skyplus.agency.domain.AbstractModel;
import com.skyplus.agency.domain.User;
import com.skyplus.agency.domain.enumeration.TokenType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

/**
 * Created by wladek on 7/22/15.
 */
@Entity
public class Tokens extends AbstractModel {
    private String tokenString;
    private boolean used;
    @Enumerated(EnumType.STRING)
    private TokenType tokenType;

    @ManyToOne
    private User user;

    public String getTokenString() {
        return tokenString;
    }

    public void setTokenString(String tokenString) {
        this.tokenString = tokenString;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    public TokenType getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
