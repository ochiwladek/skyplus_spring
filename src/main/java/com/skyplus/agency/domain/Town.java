package com.skyplus.agency.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.List;

/**
 * Created by wladek on 5/16/15.
 */
@Entity
public class Town extends AbstractModel{
    @NotEmpty(message = "You must provide a name")
    private String name;

    @Transient
    private String action;

    @Transient
    private Long townId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }

}
