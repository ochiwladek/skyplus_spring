package com.skyplus.agency.domain;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.util.List;

/**
 * Created by wladek on 7/8/15.
 */
@Entity
public class Currency extends AbstractModel {
    @NotEmpty(message = "Give name")
    private String name;
    @NotEmpty(message = "Give alias e.g KSH , USD")
    private String alias;
    @NotEmpty(message = "Give symbol e.g Ksh , $")
    private String symbol;

    @Transient
    private Long currencyId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }
}
