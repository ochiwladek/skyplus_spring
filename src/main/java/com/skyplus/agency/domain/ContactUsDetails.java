package com.skyplus.agency.domain;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by wladek on 4/27/15.
 */
public class ContactUsDetails {
    @NotEmpty(message = "Please provide your name")
    private String name;
    @NotEmpty(message = "Please provide your email address")
    private String fromEmail;
    @NotEmpty(message = "Please provide an email subject/title")
    private String title;
    @NotEmpty(message = "You must write a message")
    private String message;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
