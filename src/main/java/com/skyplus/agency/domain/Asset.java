package com.skyplus.agency.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created by wladek on 4/26/15.
 */
@Entity
public class Asset extends AbstractModel{
    private String name;

    @OneToOne(mappedBy = "asset" , fetch = FetchType.LAZY)
    private Property property;

    @OneToOne(mappedBy = "asset" , fetch = FetchType.LAZY)
    private Section section;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }
}
