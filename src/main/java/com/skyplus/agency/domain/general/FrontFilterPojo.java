package com.skyplus.agency.domain.general;

/**
 * Created by wladek on 7/21/15.
 */
public class FrontFilterPojo {
    private Long townId;
    private String propertyType; // either bungalow, messionette, apartment, office , land
    private Long subCategoryId;
    private Long categoryId;

    public Long getTownId() {
        return townId;
    }

    public void setTownId(Long townId) {
        this.townId = townId;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
