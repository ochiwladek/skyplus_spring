package com.skyplus.agency.domain.general;

import org.hibernate.validator.constraints.NotEmpty;

import java.beans.Transient;

/**
 * Created by wladek on 7/29/15.
 */
public class PasswordRecoveryPojo {
    @NotEmpty(message = "Please give a password")
   private String password;
    @NotEmpty(message = "Please give a confirmation password")
   private String password2;

    private String token;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
