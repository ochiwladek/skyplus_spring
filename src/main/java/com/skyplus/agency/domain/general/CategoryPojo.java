package com.skyplus.agency.domain.general;

/**
 * Created by wladek on 5/27/15.
 */
public class CategoryPojo {
    String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
