package com.skyplus.agency.domain.general;

/**
 * Created by wladek on 6/30/15.
 */
public class statusPojo {
    public Long idNumber;
    public String status;

    public Long getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Long idNumber) {
        this.idNumber = idNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
