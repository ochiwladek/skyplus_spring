package com.skyplus.agency.domain.land;

import com.skyplus.agency.domain.AbstractModel;
import com.skyplus.agency.domain.enumeration.InquiryStatus;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;

/**
 * Created by wladek on 1/3/10.
 */
@Entity
public class LandInquiry extends AbstractModel{
    @NotEmpty(message = "Please provide an email")
    @Email(message = "Please provide a valid email address")
    private String email;

    @NotEmpty(message = "Cannot be left blank")
    @Length(min = 10 , max = 240 , message = "Must be a at least 10 characters long")
    private String title;

    @NotEmpty(message = "This field can't be blank")
    @Length(min = 25 , message = "Must contain a minimum of 25 words")
    private String detail;

    @Enumerated(EnumType.STRING)
    private InquiryStatus status;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public InquiryStatus getStatus() {
        return status;
    }

    public void setStatus(InquiryStatus status) {
        this.status = status;
    }
}
