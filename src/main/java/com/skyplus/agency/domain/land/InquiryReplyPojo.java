package com.skyplus.agency.domain.land;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by wladek on 8/26/15.
 */
public class InquiryReplyPojo {
    private Long inquiryId;
    private String toEmail;
    private String subject;
    @NotEmpty(message = "Cannot be left blank")
    private String email;

    public Long getInquiryId() {
        return inquiryId;
    }

    public void setInquiryId(Long inquiryId) {
        this.inquiryId = inquiryId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
