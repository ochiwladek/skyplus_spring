package com.skyplus.agency.domain.land;

/**
 * Created by wladek on 8/26/15.
 */
public class InquiryFilterPojo {
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
