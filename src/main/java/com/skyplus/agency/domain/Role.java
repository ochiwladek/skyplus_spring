package com.skyplus.agency.domain;

import javax.persistence.Entity;

/**
 * @author Keeun Baik
 */
@Entity
public class Role extends AbstractModel{
    private String name;
    private boolean active;
}
