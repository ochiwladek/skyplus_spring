package com.skyplus.agency.config;

/**
 * Created by wladek on 4/24/15.
 */
import java.net.URI;
import java.net.URISyntaxException;
import javax.sql.DataSource;

import liquibase.integration.spring.SpringLiquibase;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class PersistenceJPAConfig {
    private final Logger log = LoggerFactory.getLogger(PersistenceJPAConfig.class);
    @Bean
    public DataSource dataSource() throws URISyntaxException{
        String databaseUrl = System.getenv("DATABASE_URL");
        String amazon = System.getenv("AMAZON");
        if (StringUtils.isNotBlank(databaseUrl)) {
            return herokuDataSource(databaseUrl);
        }else if (StringUtils.isNotBlank(amazon)){
            return amazonDataSource();
        } else {
            return localDataSource();
        }
    }

    private DataSource localDataSource() {
        String username = "postgres";
        String password = "";
        String dbUrl = "jdbc:postgresql://localhost:5432/skyplus";
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }
    private DataSource amazonDataSource() {
        String username = "skyplus";
        String password = "binary2015";
        String dbUrl = "jdbc:postgresql://localhost:5432/skyplus_production";
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }

    private DataSource herokuDataSource(String databaseUrl)
            throws URISyntaxException {
        URI dbUri = new URI(databaseUrl);
        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        return dataSource;
    }
    @Bean
    public SpringLiquibase liquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setChangeLog("classpath:db/changelog.json");
        liquibase.setContexts("development, production");
        log.debug("Configuring Liquibase Migartion");
        return liquibase;
    }

}

