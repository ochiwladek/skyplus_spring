package com.skyplus.agency.service;

import com.skyplus.agency.domain.enumeration.UserState;
import com.skyplus.agency.service.token.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.skyplus.agency.domain.User;
import com.skyplus.agency.domain.enumeration.UserRole;
import com.skyplus.agency.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Keeun Baik
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired UserRepository repository;
    @Autowired TokenService tokenService;

    @Autowired PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User addNewUser(User user) {

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setUserRole(UserRole.USER);
        user.setUserState(UserState.ACTIVE);
        User newUser = repository.save(user);

        tokenService.sendAccountActivationToken(newUser);

        return newUser;
    }

    @Override
    public void login(User user) {
        UserDetailsImpl ud = new UserDetailsImpl(user);
        Authentication authentication = new UsernamePasswordAuthenticationToken(ud, ud.getPassword(), ud.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Override
    @Transactional
    public User saveUser(User user) {
        return repository.save(user);
    }

    @Override
    @Transactional
    public User findByEmail(String email) {
        return repository.findByEmail(email);
    }
}
