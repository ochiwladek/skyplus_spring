package com.skyplus.agency.service;

import com.skyplus.agency.domain.Property;
import com.skyplus.agency.domain.Section;
import com.skyplus.agency.domain.enumeration.Category;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by wladek on 1/11/16.
 */
public interface PropertyService {
    public Property create(Property property);
    public Property update(Property property);
    public Property getOne(Long id);
    public List<Property> getAll();
    public boolean delete(Long id);
    public List<Property> getByCategoryAndSubCategory(Category category , String subcategory);
    public List<Property> getByCategory(Category category);
    public Page<Property> findAll(int page , int size);
    public Page<Property> getByPaged(String category , String subcategory , int page , int size);
    public Section addSection(Section section);
}
