package com.skyplus.agency.service.token;

import com.skyplus.agency.domain.User;
import com.skyplus.agency.domain.enumeration.TokenType;
import com.skyplus.agency.domain.enumeration.UserState;
import com.skyplus.agency.domain.general.PasswordRecoveryPojo;
import com.skyplus.agency.domain.tokens.Tokens;
import com.skyplus.agency.repository.token.TokenRepo;
import com.skyplus.agency.service.ApplicationMailer;
import com.skyplus.agency.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.UUID;

/**
 * Created by wladek on 7/24/15.
 */
@Service
public class TokenServiceImpl implements TokenService{
    @Autowired TokenRepo tokenRepo;
    @Autowired ApplicationMailer applicationMailer;
    @Autowired Environment env;
    @Autowired UserService userService;
    @Autowired PasswordEncoder passwordEncoder;

    Logger log = LoggerFactory.getLogger(TokenService.class);

    @Override
    @Transactional
    public String sendAccountActivationToken(User user) {
        Tokens token = new Tokens();

        String tokenString = generateToken();

        token.setUser(user);
        token.setTokenString(tokenString);
        token.setTokenType(TokenType.ACCOUNT_ACTIVATION);
        token.setUsed(false);

        Tokens savedToken = saveToken(token);


        String subject = " SKY PLUS ACCOUNT ACTIVATION ";
        String url = env.getProperty("spring.mail.baseUrl");
        String emailBody = " " +
                "Click on this link to activate your account > "
                +url+
                "/front/account/activate-account/"+savedToken.getId()+"/"
                +savedToken.getTokenString();
        String recipient = user.getEmail();

        return sendEmail(recipient , subject , emailBody);
    }

    @Override
    @Transactional
    public String sendPasswordResetToken(String email) throws NullPointerException{
        User userInDb = userService.findByEmail(email);

        try {
            if (userInDb != null){
                Tokens token = new Tokens();

                String tokenString = generateToken();

                token.setUser(userInDb);
                token.setTokenString(tokenString);
                token.setTokenType(TokenType.PASSWORD_RECOVERY);
                token.setUsed(false);

                Tokens savedToken = saveToken(token);


                String subject = " SKY PLUS PASSWORD RESET ";
                String url = env.getProperty("spring.mail.baseUrl");
                String emailBody = " " +
                        "Click on this link to RESET your password > "
                        +url+
                        "/front/account/password-recover/"+savedToken.getId()+"/"
                        +savedToken.getTokenString();
                String recipient = userInDb.getEmail();

                return sendEmail(recipient , subject , emailBody);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        return "Error";

    }

    @Override
    @Transactional
    public String activateAccount(Long tokenId, String tokenString) throws NullPointerException{
        Tokens tokenInDb = tokenRepo.findByTokenString(tokenString);

        String tokenMessage = "Invalid activation key";

        try {
            if (tokenInDb != null){
                if (!tokenInDb.isUsed() && tokenInDb.getTokenType().equals(TokenType.ACCOUNT_ACTIVATION)) {
                    User tokenUser = tokenInDb.getUser();
                    String hack = tokenUser.getPassword();//Password validation Hack
                    tokenUser.setPassword2(hack);
                    tokenUser.setUserState(UserState.ACTIVE);
                    userService.saveUser(tokenUser);
                    tokenInDb.setUsed(true);
                    saveToken(tokenInDb);

                    tokenMessage = "Success";

                } else if (tokenInDb.isUsed()) {

                    tokenMessage = "Active Account";
                }
            }else {
                tokenMessage = "Invalid activation key";
            }
        }catch (NullPointerException e){
            tokenMessage = "Invalid activation key";
        }

        return tokenMessage;
    }

    @Override
    @Transactional
    public String resetPassword(PasswordRecoveryPojo passwordRecoveryPojo) {
        Tokens tokenInDb = tokenRepo.findByTokenString(passwordRecoveryPojo.getToken());
        User tokenUser = tokenInDb.getUser();

        String newPassword = passwordEncoder.encode(passwordRecoveryPojo.getPassword());
        tokenUser.setPassword(newPassword);
        tokenUser.setPassword2(newPassword);

        userService.saveUser(tokenUser);

        return "Password Reset Success";
    }

    //generate a random string for the token
    public String generateToken() {
        String token = UUID.randomUUID().toString();
        return token;
    }

    //send the email
    public String sendEmail(String to, String subject,String emailBody){
        log.info("======================Token mail ==================  "+emailBody);
        applicationMailer.sendActivationMail(to , subject , emailBody);
        return "Success";
    }

    //save the token generated
    public Tokens saveToken(Tokens tokens){
        return tokenRepo.save(tokens);
    }
}
