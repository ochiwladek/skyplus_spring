package com.skyplus.agency.service.token;

import com.skyplus.agency.domain.User;
import com.skyplus.agency.domain.general.PasswordRecoveryPojo;

/**
 * Created by wladek on 7/24/15.
 */
public interface TokenService {
    public String sendAccountActivationToken(User user);
    public String sendPasswordResetToken(String email);
    public String activateAccount(Long tokenId , String tokenString);
    public String resetPassword(PasswordRecoveryPojo passwordRecoveryPojo);
}
