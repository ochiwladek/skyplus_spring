package com.skyplus.agency.service;

/**
 * Created by wladek on 7/13/15.
 */
public interface SmsService {
    public String sendMessage(String to,String sms) throws Exception;
}
