package com.skyplus.agency.service;

import com.skyplus.agency.domain.ContactUsDetails;

/**
 * Created by wladek on 4/27/15.
 */
public interface ContactUsService {
    public String contactUs(ContactUsDetails contactUsDetails);
}
