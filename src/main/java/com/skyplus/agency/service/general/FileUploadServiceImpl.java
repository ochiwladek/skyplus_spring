package com.skyplus.agency.service.general;

import com.skyplus.agency.config.AwsConfig;
import com.skyplus.agency.domain.Asset;
import com.skyplus.agency.repository.admin.category.AssetRepository;
import com.skyplus.agency.service.cloud.S3Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;

/**
 * Created by wladek on 5/16/15.
 */
@Service
public class FileUploadServiceImpl implements FileUploadService{
    Logger logger = LoggerFactory.getLogger(FileUploadServiceImpl.class);

    @Autowired AssetRepository assetRepository;
    @Autowired AwsConfig awsConfig;
    @Autowired S3Service s3Service;
    @Autowired Environment env;

    @Override
    @Transactional
    public Asset uploadImage(MultipartFile multipartFile) {
        Asset asset = new Asset();
        byte[] bytes;

        try {
            byte[] array = new byte[9]; // length is bounded by 7
            new Random().nextBytes(array);
            String prefix = UUID.randomUUID().toString();
            String fileName = multipartFile.getOriginalFilename().replaceAll("\\s", "_");
            String name = prefix+"/"+fileName;
            bytes = multipartFile.getBytes();
            InputStream inputStream = new ByteArrayInputStream(bytes);
            s3Service.putAsset(prefix,fileName,inputStream);
            String fullName = awsConfig.getBaseUrl()+"/"+
                    awsConfig.getBucket()+"/"+name;

            if (Objects.equals(env.getProperty("spring.profiles.active"), "prod")){
                fullName= awsConfig.getBaseUrl()+"/"+
                        awsConfig.getBucket()+"/"+name;
            }
            asset.setName(fullName);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return assetRepository.save(asset);
    }
}

