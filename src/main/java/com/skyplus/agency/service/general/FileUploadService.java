package com.skyplus.agency.service.general;

import com.skyplus.agency.domain.Asset;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by wladek on 5/16/15.
 */
public interface FileUploadService {
    public Asset uploadImage(MultipartFile multipartFile);
}
