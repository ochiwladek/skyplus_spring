package com.skyplus.agency.service;

import com.amazonaws.util.json.JSONArray;
import com.skyplus.agency.service.api.SmsGateWayApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wladek on 7/13/15.
 */
@Service
public class SmsServiceImpl implements SmsService{
    Logger logger = LoggerFactory.getLogger(SmsServiceImpl.class);
    @Autowired SmsGateWayApi smsGateWayApi;
    @Override
    public String sendMessage(String to , String sms) throws Exception{
        JSONArray result = smsGateWayApi.sendMessage(to, sms);

        logger.warn("========================================================"+result.toString());

        return "Success";
    }
}
