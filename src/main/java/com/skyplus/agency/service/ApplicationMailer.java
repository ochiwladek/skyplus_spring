package com.skyplus.agency.service;

import com.skyplus.agency.domain.ContactUsDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by wladek on 09/01/15.
 */
@Service
public class ApplicationMailer {
    @Autowired JavaMailSender javaMailSender;
    /**
     * This method will send compose and send the message
     * */
    public void sendMail(String to, String subject, String body)
    {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom("admin@skyplusagencies.com");
        message.setSubject(subject);
        message.setText(body);
        javaMailSender.send(message);

        System.out.println("====================================================");
        System.out.println("Email message "+message);
        System.out.println("====================================================");
    }

    public void sendConfirmationEmail(String to){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo("wladek.airo@gmail.com");
        mailMessage.setReplyTo("support@skyplus.co.ke");
        mailMessage.setFrom("saowwladek@gmail.com");
        mailMessage.setSubject("Lorem ipsum");
        mailMessage.setText("Lorem ipsum dolor sit amet [...]");
        javaMailSender.send(mailMessage);
    }
    public SimpleMailMessage send() {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo("wladek.airo@gmail.com");
        mailMessage.setReplyTo("support@skyplusagencies.com");
        mailMessage.setFrom("saowwladek@gmail.com");
        mailMessage.setSubject("Lorem ipsum");
        mailMessage.setText("Lorem ipsum dolor sit amet [...]");
        javaMailSender.send(mailMessage);
        return mailMessage;
    }

    public SimpleMailMessage sendActivationMail(String to, String subject, String body) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(to);
        mailMessage.setReplyTo("noReply@skyplusagencies.com");
        mailMessage.setFrom("admin@skyplusagencies.com");
        mailMessage.setSubject(subject);
        mailMessage.setText(body);
        javaMailSender.send(mailMessage);
        return mailMessage;
    }

    public String sendContactUsMail(ContactUsDetails contactUsDetails){

        Date date = new Date();

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom(contactUsDetails.getFromEmail());
        simpleMailMessage.setTo("info@skyplusagencies.com");
        simpleMailMessage.setSubject(contactUsDetails.getTitle());
        simpleMailMessage.setReplyTo(contactUsDetails.getFromEmail());
        simpleMailMessage.setText(contactUsDetails.getMessage());
        simpleMailMessage.setSentDate(date);
        javaMailSender.send(simpleMailMessage);

        return "Success";
    }
}
