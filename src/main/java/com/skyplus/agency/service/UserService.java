package com.skyplus.agency.service;

import com.skyplus.agency.domain.User;

/**
 * @author Keeun Baik
 */
public interface UserService {

    User addNewUser(User user);

    void login(User user);

    User saveUser(User user);

    User findByEmail(String email);
}
