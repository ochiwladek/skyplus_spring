package com.skyplus.agency.service;

import com.skyplus.agency.domain.Asset;
import com.skyplus.agency.domain.Property;
import com.skyplus.agency.domain.Section;
import com.skyplus.agency.domain.enumeration.Category;
import com.skyplus.agency.repository.PropertyRepo;
import com.skyplus.agency.repository.SectionRepo;
import com.skyplus.agency.service.general.FileUploadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by wladek on 1/11/16.
 */
@Service
public class PropertyServiceImpl implements PropertyService{
    Logger logger = LoggerFactory.getLogger(PropertyServiceImpl.class);

    @Autowired
    PropertyRepo propertyRepo;
    @Autowired
    FileUploadService fileUploadService;
    @Autowired
    SectionRepo sectionRepo;

    @Override
    public Property create(Property property) {
        logger.info(" ++ CREATE PROPERTY +++ ");
        Asset asset = fileUploadService.uploadImage(property.getMultipartFile());

        logger.info(" ++ ASSET ID +++ "+asset.getId());

        property.setAsset(asset);

        return propertyRepo.save(property);
    }

    @Override
    public Property getOne(Long id) {
        return propertyRepo.findOne(id);
    }

    @Override
    public List<Property> getAll() {
        return propertyRepo.findAll();
    }

    @Override
    public boolean delete(Long id) {
        propertyRepo.delete(id);
        return true;
    }

    @Override
    public List<Property> getByCategoryAndSubCategory(Category category, String subcategory) {
        return propertyRepo.findByCategoryAndSubcategory(category , subcategory);
    }

    @Override
    public List<Property> getByCategory(Category category) {
        return propertyRepo.findByCategory(category);
    }

    @Override
    public Property update(Property property) {
        logger.info(" ++ UPDATING PROPERTY +++ ");


        Property propertyInDb = getOne(property.getId());
        propertyInDb.setName(property.getName());
        propertyInDb.setCategory(Category.valueOf(property.getCategoryId().toUpperCase()));
        propertyInDb.setSubcategory(property.getSubcategory());
        propertyInDb.setCost(property.getCost());

        if (property.getMultipartFile() != null){

            Asset asset = fileUploadService.uploadImage(property.getMultipartFile());

            logger.info(" ++ ASSET ID +++ "+asset.getId());

            propertyInDb.setAsset(asset);
        }

        return propertyRepo.save(property);
    }

    @Override
    public Page<Property> findAll(int page, int size) {
        page = page -1;
        PageRequest pageRequest = new PageRequest(page , size);
        return propertyRepo.findAll(pageRequest);
    }

    @Override
    public Page<Property> getByPaged(String category, String type, int page, int size) {

        page = page -1;
        PageRequest pageRequest = new PageRequest(page , size);

        Page<Property> propertyPage = propertyRepo.findByCategoryAndType(Category.valueOf(category.toUpperCase()),
                type , pageRequest);
        logger.info(" **************** CATEGORY ************** "+ category.toUpperCase()+" SIZE "+propertyPage.getContent().size());
        return propertyPage;
    }

    @Override
    public Section addSection(Section section) {
        Asset asset = null;

        if (section.getMultipartFile() != null){
            asset = fileUploadService.uploadImage(section.getMultipartFile());
        }

        if (asset != null){
            section.setAsset(asset);
        }

        section.setProperty(getOne(section.getPropertyId()));

        return sectionRepo.save(section);
    }
}
