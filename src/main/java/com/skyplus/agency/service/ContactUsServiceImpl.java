package com.skyplus.agency.service;

import com.skyplus.agency.domain.ContactUsDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by wladek on 4/27/15.
 */
@Service
public class ContactUsServiceImpl implements ContactUsService{
    Logger logger = LoggerFactory.getLogger(ContactUsServiceImpl.class);
    @Autowired ApplicationMailer applicationMailer;
    @Override
    public String contactUs(ContactUsDetails contactUsDetails) {
        logger.info("Method contactUs sending contact us mail");
        String message = applicationMailer.sendContactUsMail(contactUsDetails);
        return message;
    }
}
