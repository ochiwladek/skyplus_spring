package com.skyplus.agency.service.faqs;

import com.skyplus.agency.domain.Faqs;
import com.skyplus.agency.repository.faqs.FaqsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by wladek on 8/28/15.
 */
@Service
public class FaqsServiceImpl implements FaqsService{
    @Autowired FaqsRepo faqsRepo;

    @Override
    @Transactional
    public Faqs create(Faqs faqs) {
        return faqsRepo.save(faqs);
    }

    @Override
    @Transactional
    public Faqs findOne(Long id) {
        return faqsRepo.findOne(id);
    }

    @Override
    @Transactional
    public void delete(Faqs faqs) {
        faqsRepo.delete(faqs);
    }

    @Override
    @Transactional
    public List<Faqs> findAll() {
        return faqsRepo.findAll();
    }
}
