package com.skyplus.agency.service.faqs;

import com.skyplus.agency.domain.Faqs;

import java.util.List;

/**
 * Created by wladek on 8/28/15.
 */
public interface FaqsService {
    public Faqs create(Faqs faqs);
    public Faqs findOne(Long id);
    public void delete(Faqs faqs);
    public List<Faqs> findAll();
}
